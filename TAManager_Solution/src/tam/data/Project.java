package tam.data;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a Teaching Assistant for the table of TAs.
 * 
 * @author Richard McKenna
 */
public class Project<E extends Comparable<E>> implements Comparable<E>  {

    public static ArrayList<Project> buildProjectList(TAData dataManager) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final StringProperty name;
    private final StringProperty color;
    private final StringProperty textColor;
    private final StringProperty link;

    /**
     * Constructor initializes both the TA name and email.
     */
    public Project(String initName, String initColor, String initTextColor, String initLink) {
        name = new SimpleStringProperty(initName);
        color = new SimpleStringProperty(initColor);
        textColor = new SimpleStringProperty(initTextColor);
        link = new SimpleStringProperty(initLink);
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES
    
    public String getName(){
        return name.get();
    }
    public String getColor(){
        return color.get();
    }
    public String getTextColor(){
        return textColor.get();
    }
    public String getLink(){
        return link.get();
    }
    
    @Override
    public int compareTo(E otherTA) {
        return getName().compareTo(((Project)otherTA).getName());
    }
    
    @Override
    public String toString() {
        return name.getValue();
    }

    public void setName(String newName) {
        name.set(newName);
    }

    public void setLink(String newLink) {
        link.set(newLink);
    }

    public void setColor(String newColor) {
        color.set(newColor);
    }

    public void setTextColor(String newTextColor) {
        textColor.set(newTextColor);
    }
}