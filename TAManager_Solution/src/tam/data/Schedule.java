package tam.data;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a Teaching Assistant for the table of TAs.
 * 
 * @author Richard McKenna
 */
public class Schedule<E extends Comparable<E>> implements Comparable<E>  {

    public static ArrayList<Schedule> buildScheduleList(TAData dataManager) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final StringProperty type;
    private final StringProperty date;
    private final StringProperty title;
    private final StringProperty topic;
    private final StringProperty link;
    private final StringProperty criteria;
    private final StringProperty time;

    /**
     * Constructor initializes both the TA name and email.
     */
    public Schedule(String initType, String initDate, String initTitle, String initTopic, String initLink, String initCriteria, String initTime) {
        type = new SimpleStringProperty(initType);
        date = new SimpleStringProperty(initDate);
        title = new SimpleStringProperty(initTitle);
        topic = new SimpleStringProperty(initTopic);
        link = new SimpleStringProperty(initLink);
        criteria = new SimpleStringProperty(initCriteria);
        time = new SimpleStringProperty(initTime);
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES
    
    public String getType(){
        return type.get();
    }
    public String getDate(){
        return date.get();
    }
    public String getTitle(){
        return title.get();
    }
    public String getTopic(){
        return topic.get();
    }
    public String getLink(){
        return link.get();
    }
    public String getCriteria(){
        return criteria.get();
    }
    public String getTime(){
        return time.get();
    }
    
    @Override
    public int compareTo(E otherTA) {
        return getTitle().compareTo(((Schedule)otherTA).getTitle());
    }
    
    @Override
    public String toString() {
        return title.getValue();
    }

    public void setType(String newType) {
        type.set(newType);
    }

    public void setDate(String newDate) {
        date.set(newDate);
    }

    public void setTitle(String newTitle) {
        title.set(newTitle);
    }

    public void setTopic(String newTopic) {
        topic.set(newTopic);
    }

    public void setLink(String newLink) {
        link.set(newLink);
    }

    public void setCriteria(String newCriteria) {
        criteria.set(newCriteria);
    }

    public void setTime(String newLink) {
        time.set(newLink);
    }
}