package tam.data;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a Teaching Assistant for the table of TAs.
 * 
 * @author Richard McKenna
 */
public class Recitation<E extends Comparable<E>> implements Comparable<E>  {
    // THE TABLE WILL STORE TA NAMES AND EMAILS
    StringProperty section;
    StringProperty instructor;
    StringProperty date;
    StringProperty location;
    StringProperty TAOne;
    StringProperty TATwo;

    /**
     * Constructor initializes both the TA name and email.
     */
    public Recitation(String initSection, String initInstructor, String initDate, String initLocation, String initTAOne, String initTATwo) {
        section = new SimpleStringProperty(initSection);
        instructor = new SimpleStringProperty(initInstructor);
        date = new SimpleStringProperty(initDate);
        location = new SimpleStringProperty(initLocation);
        TAOne = new SimpleStringProperty(initTAOne);
        TATwo = new SimpleStringProperty(initTATwo);
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES
    
    public String getSection(){
        return section.get();
    }
    public void setSection(String initSection){
        section.set(initSection);
    }
    public String getInstructor(){
        return instructor.get();
    }
    
    public void setInstructor(String initSection){
        instructor.set(initSection);
    }
    public String getDate(){
        return date.get();
    }
    
    public void setDate(String initSection){
        date.set(initSection);
    }
    public String getLocation(){
        return location.get();
    }
    
    public void setLocation(String initSection){
        location.set(initSection);
    }
    public String getTAOne(){
        return TAOne.get();
    }
    
    public void setTAOne(String initSection){
        TAOne.set(initSection);
    }
    public String getTATwo(){
        return TATwo.get();
    }
    
    public void setTATwo(String initSection){
        TATwo.set(initSection);
    }
    
    
    @Override
    public int compareTo(E otherTA) {
        return getDate().compareTo(((Recitation)otherTA).getDate());
    }
    
    @Override
    public String toString() {
        return date.getValue();
    }
}