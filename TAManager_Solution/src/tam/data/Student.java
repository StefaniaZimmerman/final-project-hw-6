package tam.data;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a Teaching Assistant for the table of TAs.
 * 
 * @author Richard McKenna
 */
public class Student<E extends Comparable<E>> implements Comparable<E>  {

    public static ArrayList<Student> buildStudentList(TAData dataManager) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final StringProperty firstName;
    private final StringProperty lastName;
    private final StringProperty teams;
    private final StringProperty role;
    private final StringProperty fullName;

    /**
     * Constructor initializes both the TA name and email.
     */
    public Student(String initFirstName, String initLastName, String initTeams, String initRole) {
        firstName = new SimpleStringProperty(initFirstName);
        lastName = new SimpleStringProperty(initLastName);
        teams = new SimpleStringProperty(initTeams);
        role = new SimpleStringProperty(initRole);
        fullName = new SimpleStringProperty(initFirstName+initLastName);
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES
    public String getFullName(){
        return fullName.get();
    }
    public void setFullName(String first,String last){
        fullName.set(first+last);
    }
    public String getFirstName(){
        return firstName.get();
    }
    public String getLastName(){
        return lastName.get();
    }
    public String getTeams(){
        return teams.get();
    }
    public String getRole(){
        return role.get();
    }
    
    @Override
    public int compareTo(E otherTA) {
        return getFirstName().compareTo(((Student)otherTA).getFirstName());
    }
    
    @Override
    public String toString() {
        return firstName.getValue();
    }

    public void setFirstName(String newFirstName) {
        firstName.set(newFirstName);}

    public void setLastName(String newLastName) {
        lastName.set(newLastName);}

    public void setTeam(String newTeam) {
        teams.set(newTeam);
    }

    public void setRole(String newRole) {
        role.set(newRole);}
}