package tam.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import tam.TAManagerApp;
import tam.data.Project;
import tam.data.Recitation;
import tam.data.Schedule;
import tam.data.Student;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.workspace.TAWorkspace;
//import tam.workspace.TAWorkspace;

/**
 * This class serves as the file component for the TA
 * manager app. It provides all saving and loading 
 * services for the application.
 * 
 * @author Richard McKenna
 */
public class TAFiles implements AppFileComponent {
    // THIS IS THE APP ITSELF
    TAManagerApp app;
    
    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_EMAIL = "email";
    static final String JSON_UNDERGRAD="undergrad";
    private String JSON_TAS="teachingAssistants:";
    //For HW 5
    //Course details
    static final String JSON_SUBJECT="subject";
    static final String JSON_COURSE_NUMBER="courseNumber";
    static final String JSON_SEMESTER="semester";
    static final String JSON_YEAR="year";
    static final String JSON_COURSE_TITLE="courseTitle";
    static final String JSON_INSTRUCTOR_NAME="instructorName";
    static final String JSON_INSTRUCTOR_HOME="instructorHome";
    static final String JSON_EXPORT_DIR="exportDirectory";    
    static final String JSON_TEMPLATE_DIR="templateDirectory";
    static final String JSON_BANNER="bannerImage";
    static final String JSON_LEFT_FOOTER="leftFooterImage";
    static final String JSON_RIGHT_FOOTER="rightFooterImage";
    static final String JSON_STYLESHEET="stylesheet";
    //See TA Data above
    //Recitations
    static final String JSON_RECITATION="recitations";
    static final String JSON_SECTION_RECITATION="recitationSection";
    static final String JSON_INSTRUCTOR_RECITATION="recitationInstructor";
    static final String JSON_DATE_RECITATION="recitationDate";
    static final String JSON_LOCATION_RECITATION="recitationLocation";
    static final String JSON_TA_ONE="recitationTAOne";
    static final String JSON_TA_TWO="recitationTATwo";
    //Schedule
    static final String JSON_SCHEDULE = "schedule";
    static final String JSON_STARTING_DATE="startingDate";
    static final String JSON_ENDING_DATE="endingDate";
    static final String JSON_TYPE="type";
    static final String JSON_DATE_SCHEDULE="scheduleDate";
    static final String JSON_TITLE_SCHEDULE="scheduleTitle";
    static final String JSON_TOPIC="topic";
    static final String JSON_SCHEDULE_LINK="scheduleLink";
    static final String JSON_SCHEDULE_CRITERIA="scheduleCriteria";
    static final String JSON_SCHEDULE_TIME="scheduleTime";
    
    //Project Data
    static final String JSON_PROJECTS = "projects";
    static final String JSON_NAME_PROJECT="projectName";
    static final String JSON_COLOR="color";
    static final String JSON_TEXT_COLOR="textColor";
    static final String JSON_LINK="link";
        //Student part
    static final String JSON_STUDENT="students";
    static final String JSON_FIRST_NAME="firstName";
    static final String JSON_LAST_NAME="lastName";
    static final String JSON_TEAMS="teams";
    static final String JSON_ROLE="role";
    

    public TAFiles(){

    }
    public TAFiles(TAManagerApp initApp) {
        app = initApp;
    }
    //Creates all the different JSON files
    public JsonObject getCourseInfoJSON(AppDataComponent data){
        //Course info
        TAData dataManager = (TAData)data;
	JsonObject courseInfo =  Json.createObjectBuilder()
                .add(JSON_SUBJECT," " + dataManager.getSubject())
                .add(JSON_COURSE_NUMBER,"" + dataManager.getCourseNumber())
                .add(JSON_SEMESTER,"" + dataManager.getSemester())
                .add(JSON_YEAR,"" + dataManager.getYear())
                .add(JSON_COURSE_TITLE,"" + dataManager.getCourseTitle())
                .add(JSON_INSTRUCTOR_NAME,"" + dataManager.getInstructorName())
                .add(JSON_INSTRUCTOR_HOME,"" + dataManager.getInstructorHome())
                .add(JSON_EXPORT_DIR,"" + dataManager.getExportDirectory())
                .add(JSON_TEMPLATE_DIR,"" + dataManager.getTemplateDirectory())
                .add(JSON_BANNER,"" + dataManager.getBannerImage())
                .add(JSON_LEFT_FOOTER,"" + dataManager.getLeftFooterImage())
                .add(JSON_RIGHT_FOOTER,"" + dataManager.getRightFooterImage())
                .add(JSON_STYLESHEET,"" + dataManager.getStylesheet()).build();
        return courseInfo;
    }
    public JsonObject getTAInfoJSON(AppDataComponent data){
        TAData dataManager = (TAData)data;
        	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {	    
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_UNDERGRAD, ta.getUndergrad())
                    .build();
	    taArrayBuilder.add(taJson);
	}
	JsonArray TAsArray = taArrayBuilder.build();
        //Build an array of undergrad TAs
        JsonArrayBuilder undergradTAArrayBuilder = Json.createArrayBuilder();
        for(TeachingAssistant ta: tas){
            if(ta.getUndergrad()){	    
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_UNDERGRAD, ta.getUndergrad())
                    .build();
	    undergradTAArrayBuilder.add(taJson);
            }            
        }
        JsonArray undergradTAsArray = undergradTAArrayBuilder.build();
	// NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        JsonObject teachingAssistantInfo = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_TAS, TAsArray)
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray).build();
        return teachingAssistantInfo;
    }
    public JsonObject getRecitationInfoJSON(AppDataComponent data){
        TAData dataManager = (TAData)data;
        JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = dataManager.getRecitations();
        for (Recitation r : recitations){
            JsonObject rJson = Json.createObjectBuilder()
                    .add(JSON_SECTION_RECITATION,r.getSection())
                    .add(JSON_INSTRUCTOR_RECITATION,r.getInstructor())
                    .add(JSON_DATE_RECITATION,r.getDate())
                    .add(JSON_LOCATION_RECITATION,r.getLocation())
                    .add(JSON_TA_ONE,r.getTAOne())
                    .add(JSON_TA_TWO,r.getTATwo())
                    .build();
            recitationArrayBuilder.add(rJson);
        }
        JsonArray recitationsArray = recitationArrayBuilder.build();
        JsonObject recitationInfo = Json.createObjectBuilder()
                .add(JSON_RECITATION,recitationsArray).build();
        return recitationInfo;
    }
    public JsonObject getScheduleInfoJSON(AppDataComponent data){
        TAData dataManager = (TAData)data;
        //Build schedule Objects
        JsonArrayBuilder scheduleArrayBuilder = Json.createArrayBuilder();
        ObservableList<Schedule> schedules = dataManager.getSchedules();
        for (Schedule s : schedules){
            JsonObject sJson = Json.createObjectBuilder()
                    .add(JSON_TYPE,s.getType())
                    .add(JSON_DATE_SCHEDULE,s.getDate())
                    .add(JSON_TITLE_SCHEDULE,s.getTitle())
                    .add(JSON_TOPIC,s.getTopic())
                    .add(JSON_SCHEDULE_LINK,s.getLink())
                    .add(JSON_SCHEDULE_CRITERIA,s.getCriteria())
                    .add(JSON_SCHEDULE_TIME,s.getTime())
                    .build();
            scheduleArrayBuilder.add(sJson);
        }
        JsonArray scheduleArray = scheduleArrayBuilder.build();
        JsonObject scheduleInfo = Json.createObjectBuilder()
                .add(JSON_SCHEDULE, scheduleArray)
                .add(JSON_STARTING_DATE,"" + dataManager.getStartingDate())
                .add(JSON_ENDING_DATE,"" + dataManager.getStartingDate()).build();
        return scheduleInfo;
    }
    public JsonObject getProjectInfoJSON(AppDataComponent data){
        TAData dataManager = (TAData)data;
        JsonArrayBuilder projectArrayBuilder = Json.createArrayBuilder();
        ObservableList<Project> projects = dataManager.getProjects();
        for(Project p : projects){
            JsonObject pJson = Json.createObjectBuilder()
                    .add(JSON_NAME_PROJECT,p.getName())
                    .add(JSON_COLOR,p.getColor())
                    .add(JSON_TEXT_COLOR,p.getTextColor())
                    .add(JSON_LINK,p.getLink())
                    .build();
            projectArrayBuilder.add(pJson);
        }
        JsonArray projectArray = projectArrayBuilder.build();
        //Build students for projects
        JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
        ObservableList<Student> students = dataManager.getStudents();
        for(Student st : students){
            JsonObject stJson = Json.createObjectBuilder()
                    .add(JSON_FIRST_NAME,st.getFirstName())
                    .add(JSON_LAST_NAME,st.getLastName())
                    .add(JSON_TEAMS,st.getTeams())
                    .add(JSON_ROLE,st.getRole())
                    .build();
            studentArrayBuilder.add(stJson);
        }
        JsonArray studentArray = studentArrayBuilder.build();
              JsonObject projectInfo = Json.createObjectBuilder()
                .add(JSON_PROJECTS, projectArray)
                .add(JSON_STUDENT, studentArray).build();
        return projectInfo;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException{
	// CLEAR THE OLD DATA OUT
	TAData dataManager = (TAData)data;
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        app = (TAManagerApp)app;
        // RESET THE WORKSPACE
		app.getWorkspaceComponent().resetWorkspace();

                // RESET THE DATA
                app.getDataComponent().resetData();
                
                // NOW RELOAD THE WORKSPACE WITH THE RESET DATA
                app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());

		// MAKE SURE THE WORKSPACE IS ACTIVATED
		app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());

	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
        
        //Course Info Page
        String subject = json.getString(JSON_SUBJECT);
        dataManager.setSubject(subject);
        workspace.getSubjectBox().setValue(subject);
        
        String semester = json.getString(JSON_SEMESTER);
        dataManager.setSemester(semester);
        workspace.getSemesterBox().setValue(semester);
        
        String courseNumber = json.getString(JSON_COURSE_NUMBER);
        dataManager.setCourseNumber(courseNumber);
        workspace.getNumberBox().setValue(courseNumber);
        
        String year = json.getString(JSON_YEAR);
        dataManager.setYear(year);
        workspace.getYearBox().setValue(year);
        
        String title = json.getString(JSON_COURSE_TITLE);
        dataManager.setCourseTitle(title);
        workspace.getCourseTitleBox().setText(title);
        
        String instructorName = json.getString(JSON_INSTRUCTOR_NAME);
        dataManager.setInstructorName(instructorName);
        workspace.getInstructorNameBox().setText(instructorName);
        
        String instructorHome = json.getString(JSON_INSTRUCTOR_HOME);
        dataManager.setInstructorHome(instructorHome);
        workspace.getInstructorHomeBox().setText(instructorHome);
        
        String exportDir = json.getString(JSON_EXPORT_DIR);
        dataManager.setTemplateDirectory(exportDir);
        workspace.getExportDirectoryBox().setText(exportDir);
        
        String templateDir = json.getString(JSON_TEMPLATE_DIR);
        dataManager.setTemplateDirectory(templateDir);
        workspace.getTemplateDirectoryBox().setText(templateDir);
        
        System.out.println("Start loading Images");
        String bannerImage = json.getString(JSON_BANNER);
        dataManager.setBannerImage(bannerImage);
        File file = new File(bannerImage);
        String localUrl = file.toURI().toURL().toString();
        Image image = new Image(localUrl, false);
        workspace.bannerImageDisplay.setImage(image);
        System.out.println(bannerImage);
        
        String leftFooterImage = json.getString(JSON_LEFT_FOOTER);
        dataManager.setLeftFooterImage(leftFooterImage);      
        file = new File(leftFooterImage);
        localUrl = file.toURI().toURL().toString();
        image = new Image(localUrl, false);
        workspace.leftFooterImageDisplay.setImage(image);
        System.out.println(leftFooterImage);
        
        String rightFooterImage = json.getString(JSON_RIGHT_FOOTER);
        dataManager.setRightFooterImage(rightFooterImage);
        file = new File(rightFooterImage);
        localUrl = file.toURI().toURL().toString();
        image = new Image(localUrl, false);
        workspace.rightFooterImageDisplay.setImage(image);
        System.out.println(rightFooterImage);
        System.out.println("Images finished loading");
        
        String stylesheet = json.getString(JSON_STYLESHEET);
        dataManager.setStylesheet(stylesheet);
        workspace.getStylesheetPicker().setValue(stylesheet);
        
	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);
        System.out.println("Course Info Loaded");
        
        // NOW LOAD ALL THE TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            Boolean undergrad = jsonTA.getBoolean(JSON_UNDERGRAD);
            if(undergrad!=null){   
                dataManager.addTA(name, email,undergrad);
            }
            else{
                dataManager.addTA(name,email);
            }
        }
        System.out.println("TAs loaded");
        
        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.addOfficeHoursReservation(day, time, name);
        }
        System.out.println("Office Hours Loaded");
        
        //For the recitation page
        
        JsonArray jsonRecitationsArray = json.getJsonArray(JSON_RECITATION);
        for(int i = 0; i < jsonRecitationsArray.size();i++){
            JsonObject jsonRecitation = jsonRecitationsArray.getJsonObject(i);
            String section = jsonRecitation.getString(JSON_SECTION_RECITATION);
            String instructor = jsonRecitation.getString(JSON_INSTRUCTOR_RECITATION);
            String date = jsonRecitation.getString(JSON_DATE_RECITATION);
            String location = jsonRecitation.getString(JSON_LOCATION_RECITATION);
            String TAOne = jsonRecitation.getString(JSON_TA_ONE);
            String TATwo = jsonRecitation.getString(JSON_TA_TWO);
            dataManager.addRecitation(section, instructor, date, location, TAOne, TATwo);
        }
        System.out.println("All Recitations loaded");
        

        //For the schedule data page
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        formatter = formatter.withLocale( Locale.US ); 
        String endingDate = json.getString(JSON_ENDING_DATE);
        dataManager.setEndingDate(endingDate);
        LocalDate dateTemp = LocalDate.parse(endingDate, formatter);
        workspace.endingDateInput.setValue(dateTemp);
        String startingDate = json.getString(JSON_STARTING_DATE);
        dataManager.setStartingDate(startingDate);
        dateTemp = LocalDate.parse(startingDate, formatter);
        workspace.startingDateInput.setValue(dateTemp);
        

        JsonArray jsonSchedulesArray = json.getJsonArray(JSON_SCHEDULE);
        for(int i = 0; i < jsonSchedulesArray.size();i++){
            JsonObject jsonSchedule = jsonSchedulesArray.getJsonObject(i);
            String type = jsonSchedule.getString(JSON_TYPE);
            String date = jsonSchedule.getString(JSON_DATE_SCHEDULE);
            String scheduleTitle = jsonSchedule.getString(JSON_TITLE_SCHEDULE);
            String topic = jsonSchedule.getString(JSON_TOPIC);
            String link = jsonSchedule.getString(JSON_SCHEDULE_LINK);
            String criteria = jsonSchedule.getString(JSON_SCHEDULE_CRITERIA);
            String time = jsonSchedule.getString(JSON_SCHEDULE_TIME);
            dataManager.addSchedule(type, date, scheduleTitle, topic,link,criteria,time);
        }
        System.out.println("All Schedules Loaded");
        //For the project data page
        
        JsonArray jsonProjectsArray = json.getJsonArray(JSON_PROJECTS);
        for(int i = 0; i < jsonProjectsArray.size();i++){
            JsonObject jsonProject = jsonProjectsArray.getJsonObject(i);
            String projectName = jsonProject.getString(JSON_NAME_PROJECT);
            String color = jsonProject.getString(JSON_COLOR);
            String textColor = jsonProject.getString(JSON_TEXT_COLOR);
            String link = jsonProject.getString(JSON_LINK);
            dataManager.addProject(projectName,color,textColor,link);
        }
        
        System.out.println("All Projects Loaded");
        
        JsonArray jsonStudentsArray = json.getJsonArray(JSON_STUDENT);
        for(int i = 0; i < jsonStudentsArray.size();i++){
            JsonObject jsonStudent = jsonStudentsArray.getJsonObject(i);
            String firstName = jsonStudent.getString(JSON_FIRST_NAME);
            String lastName = jsonStudent.getString(JSON_LAST_NAME);
            String teams = jsonStudent.getString(JSON_TEAMS);
            String role = jsonStudent.getString(JSON_ROLE);
            dataManager.addStudent(firstName,lastName,teams,role);
        }
        System.out.println("All Students Loaded");
        
        // NOW RELOAD THE WORKSPACE WITH THE LOADED DATA
        
        app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());
     
    }
      
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	TAData dataManager = (TAData)data;

	// NOW BUILD THE TA JSON OBJCTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {	    
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_UNDERGRAD, ta.getUndergrad())
                    .build();
	    taArrayBuilder.add(taJson);
	}
	JsonArray TAsArray = taArrayBuilder.build();
        //Build an array of undergrad TAs
        JsonArrayBuilder undergradTAArrayBuilder = Json.createArrayBuilder();
        for(TeachingAssistant ta: tas){
            if(ta.getUndergrad()){	    
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_UNDERGRAD, ta.getUndergrad())
                    .build();
	    undergradTAArrayBuilder.add(taJson);
            }            
        }
        JsonArray undergradTAsArray = undergradTAArrayBuilder.build();
	// NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        
        //For Homework 5
        //Build recitation objects
        JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = dataManager.getRecitations();
        for (Recitation r : recitations){
            JsonObject rJson = Json.createObjectBuilder()
                    .add(JSON_SECTION_RECITATION,r.getSection())
                    .add(JSON_INSTRUCTOR_RECITATION,r.getInstructor())
                    .add(JSON_DATE_RECITATION,r.getDate())
                    .add(JSON_LOCATION_RECITATION,r.getLocation())
                    .add(JSON_TA_ONE,r.getTAOne())
                    .add(JSON_TA_TWO,r.getTATwo())
                    .build();
            recitationArrayBuilder.add(rJson);
        }
        JsonArray recitationsArray = recitationArrayBuilder.build();
        
        //Build schedule Objects
        JsonArrayBuilder scheduleArrayBuilder = Json.createArrayBuilder();
        ObservableList<Schedule> schedules = dataManager.getSchedules();
        for (Schedule s : schedules){
            JsonObject sJson = Json.createObjectBuilder()
                    .add(JSON_TYPE,s.getType())
                    .add(JSON_DATE_SCHEDULE,s.getDate())
                    .add(JSON_TITLE_SCHEDULE,s.getTitle())
                    .add(JSON_TOPIC,s.getTopic())
                    .add(JSON_SCHEDULE_LINK,s.getLink())
                    .add(JSON_SCHEDULE_CRITERIA,s.getCriteria())
                    .add(JSON_SCHEDULE_TIME,s.getTime())
                    .build();
            scheduleArrayBuilder.add(sJson);
        }
        JsonArray scheduleArray = scheduleArrayBuilder.build();
        
        //Build project objects 
        JsonArrayBuilder projectArrayBuilder = Json.createArrayBuilder();
        ObservableList<Project> projects = dataManager.getProjects();
        for(Project p : projects){
            JsonObject pJson = Json.createObjectBuilder()
                    .add(JSON_NAME_PROJECT,p.getName())
                    .add(JSON_COLOR,p.getColor())
                    .add(JSON_TEXT_COLOR,p.getTextColor())
                    .add(JSON_LINK,p.getLink())
                    .build();
            projectArrayBuilder.add(pJson);
        }
        JsonArray projectArray = projectArrayBuilder.build();
        //Build students for projects
        JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
        ObservableList<Student> students = dataManager.getStudents();
        for(Student st : students){
            JsonObject stJson = Json.createObjectBuilder()
                    .add(JSON_FIRST_NAME,st.getFirstName())
                    .add(JSON_LAST_NAME,st.getLastName())
                    .add(JSON_TEAMS,st.getTeams())
                    .add(JSON_ROLE,st.getRole())
                    .build();
            studentArrayBuilder.add(stJson);
        }
        JsonArray studentArray = studentArrayBuilder.build();
        
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_TAS, TAsArray)
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
                //For Homework 5
                //Course info
                .add(JSON_SUBJECT," " + dataManager.getSubject())
                .add(JSON_COURSE_NUMBER,"" + dataManager.getCourseNumber())
                .add(JSON_SEMESTER,"" + dataManager.getSemester())
                .add(JSON_YEAR,"" + dataManager.getYear())
                .add(JSON_COURSE_TITLE,"" + dataManager.getCourseTitle())
                .add(JSON_INSTRUCTOR_NAME,"" + dataManager.getInstructorName())
                .add(JSON_INSTRUCTOR_HOME,"" + dataManager.getInstructorHome())
                .add(JSON_EXPORT_DIR,"" + dataManager.getExportDirectory())
                .add(JSON_TEMPLATE_DIR,"" + dataManager.getTemplateDirectory())
                .add(JSON_BANNER,"" + dataManager.getBannerImage())
                .add(JSON_LEFT_FOOTER,"" + dataManager.getLeftFooterImage())
                .add(JSON_RIGHT_FOOTER,"" + dataManager.getRightFooterImage())
                .add(JSON_STYLESHEET,"" + dataManager.getStylesheet())
                //Recitation data
                .add(JSON_RECITATION,recitationsArray)
                //Schedule data
                .add(JSON_STARTING_DATE,""+dataManager.getStartingDate())
                .add(JSON_ENDING_DATE,""+dataManager.getEndingDate())
                .add(JSON_SCHEDULE,scheduleArray)
                //Projects data
                .add(JSON_PROJECTS,projectArray)
                .add(JSON_STUDENT,studentArray)
		.build();
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
	TAData dataManager = (TAData)data;

	// NOW BUILD THE TA JSON OBJCTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {	    
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_UNDERGRAD,ta.getUndergrad())
                    .build();
	    taArrayBuilder.add(taJson);
	}
	JsonArray undergradTAsArray = taArrayBuilder.build();

	// NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    
    }
}