/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam.data.Project;
import tam.data.TAData;
import tam.data.Student;


/**
 *
 * @author khurr
 */
public class UpdateProject_Transaction implements jTPS_Transaction {

    private String oldName;
    private String newName;
    private String oldLink;
    private String newLink;
    private String oldColor;
    private String newColor;
    private String oldTextColor;
    private String newTextColor;
    private TAData taData;
    private Project p;
    private TAManagerApp app; 
    private TAWorkspace transWorkspace; 

    public UpdateProject_Transaction(String orgName, String name, String orgColor, String color, String orgTextColor, String textColor, String orgLink, String link, TAData data, TAManagerApp taApp, TAWorkspace workspace) {
        oldName = orgName;
        newName = name;
        oldColor = orgColor;
        newColor = color;
        oldTextColor = orgTextColor;
        newTextColor = textColor;
        oldLink = orgLink;
        newLink = link;
        taData = data;
        p = data.getProject(orgName);
        app=taApp; 
        transWorkspace=workspace; 
    }

    @Override
    public void doTransaction() {  //Control Y 
        System.out.println("updateProject doTransaction ");
        Project project = taData.getProject(oldName);
        project.setName(newName);
        project.setLink(newLink);
        project.setColor(newColor);
        project.setTextColor(newTextColor);
        
        TAController controller = new TAController(app);
        p.setName(newName);                        // MOVED TO TRANSACTION CASE 
        p.setColor(newColor);
        p.setTextColor(newTextColor);
        p.setLink(newLink);
        
        //Go through the list of students, check to see if the team name must be changed
        ObservableList<Student> students = taData.getStudents();
        for(Student s : students){
            if(s.getTeams().equals(oldName)){
                s.setTeam(newName);
            }
        }
        
        transWorkspace.studentTable.refresh();
        transWorkspace.projectTable.refresh();
        
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("updateProject undoTransaction ");
        taData.getProject(newName).setName(oldName);
        p.setName(oldName);        // MOVED TO TRANSACTION CASE 
        p.setLink(oldLink);
        p.setColor(oldColor);
        p.setTextColor(oldTextColor);
        
        //Go through the list of students, check to see if the team name must be changed
        ObservableList<Student> students = taData.getStudents();
        for(Student s : students){
            if(s.getTeams().equals(newName)){
                s.setTeam(oldName);
            }
        }
        
        transWorkspace.studentTable.refresh();
        
        transWorkspace.projectTable.refresh();
        
        //transWorkspace.taTable.refresh();

    }

}
