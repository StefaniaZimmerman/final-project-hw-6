/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;



import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam.data.Schedule;
import tam.data.Student;
import tam.data.TAData;


/**
 *
 * @author khurr
 */
public class UpdateDateBoundries_Transaction implements jTPS_Transaction {
            
    private String oldStartingDate;
    private String newStartingDate;
    private String oldEndingDate;
    private String newEndingDate;
    private ObservableList<Schedule> removedSchedules = FXCollections.observableArrayList();
    private TAData taData;
    private TAManagerApp app; 
    private TAWorkspace transWorkspace; 

    public UpdateDateBoundries_Transaction(String orgStartingDate,String startingDate,String orgEndingDate, String endingDate, TAData data, TAManagerApp taApp, TAWorkspace workspace) {
        oldStartingDate = orgStartingDate;
        newStartingDate = startingDate;
        oldEndingDate = orgEndingDate;
        newEndingDate = endingDate;
        taData = data;
        app=taApp; 
        transWorkspace=workspace; 
    }

    @Override
    public void doTransaction() {  //Control Y 
        System.out.println("updateDateBoundries doTransaction ");
        //Set the dates
        taData.setStartingDate(newStartingDate);
        taData.setEndingDate(newEndingDate);
        //Create a list of all schedule items that fall outside the new range
        ObservableList<Schedule> schedules = taData.getSchedules();
        //Date objects
        
        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy",Locale.US);
        Date startingDate = new Date();
        Date endingDate = new Date();
        try {
            startingDate = formatter.parse(newStartingDate);
        } catch (ParseException ex) {
            Logger.getLogger(UpdateDateBoundries_Transaction.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            endingDate = formatter.parse(newEndingDate);
        } catch (ParseException ex) {
            Logger.getLogger(UpdateDateBoundries_Transaction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for(Schedule s : schedules){
            //Get the date of the current schedule item
            String tempDate = s.getDate();

            try {
                Date date = formatter.parse(tempDate);
                //Check to see if the current item falls before the starting date
                if(date.before(startingDate)){
                    //Add the schedule item to the list
                    removedSchedules.add(s);
                    //remove it
                    taData.removeSchedule(s.getTitle());
                }
                //Check to see if the item falls after the ending date
                else if(date.after(endingDate)){
                    //Add the iteam to the list
                    removedSchedules.add(s);
                    //Remove it
                    taData.removeSchedule(s.getTitle());
                }
            } catch (ParseException ex) {
                Logger.getLogger(UpdateDateBoundries_Transaction.class.getName()).log(Level.SEVERE, null, ex);
            }
            DateTimeFormatter formatterTwo = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            formatterTwo = formatterTwo.withLocale( Locale.US ); 
            LocalDate endingDateLocal = LocalDate.parse(newEndingDate, formatterTwo);
            transWorkspace.endingDateInput.setValue(endingDateLocal);
            LocalDate startingDateLocal = LocalDate.parse(newStartingDate,formatterTwo);
            transWorkspace.startingDateInput.setValue(startingDateLocal);
        }
        
        transWorkspace.scheduleTable.refresh();
        
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("updateDateBoundries undoTransaction ");
        //Restore the dates
        taData.setStartingDate(oldStartingDate);
        taData.setEndingDate(oldEndingDate);
        //Restore the values in the date picker 
        String startingDateString = taData.getStartingDate();
        String endingDateString = taData.getEndingDate();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        formatter = formatter.withLocale( Locale.US ); 
        LocalDate endingDate = LocalDate.parse(endingDateString, formatter);
        transWorkspace.endingDateInput.setValue(endingDate);
        LocalDate startingDate = LocalDate.parse(startingDateString,formatter);
        transWorkspace.startingDateInput.setValue(startingDate);
//Add the removed items back into the table
        for(Schedule s:removedSchedules){
            taData.getSchedules().add(s);
        }
        transWorkspace.scheduleTable.refresh();
        
        //transWorkspace.taTable.refresh();

    }

}
