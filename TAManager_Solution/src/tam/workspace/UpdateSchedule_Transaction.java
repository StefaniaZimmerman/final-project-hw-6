/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;


import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam.data.Project;
import tam.data.Schedule;
import tam.data.TAData;
import tam.data.TeachingAssistant;


/**
 *
 * @author khurr
 */
public class UpdateSchedule_Transaction implements jTPS_Transaction {
                  
  
    private String oldType;
    private String newType;
    private String oldDate;
    private String newDate;
    private String oldTitle;
    private String newTitle;
    private String oldTopic;
    private String newTopic;
    private String oldLink;
    private String newLink;
    private String oldCriteria;
    private String newCriteria;
    private String oldTime;
    private String newTime;
    
    private TAData taData;
    private Schedule s;
    private TAManagerApp app; 
    private TAWorkspace transWorkspace; 

    public UpdateSchedule_Transaction(String orgType, String type, String orgDate, String date, String orgTitle,
            String title, String orgTopic, String topic, String orgLink, String link, String orgCriteria,
            String criteria, String orgTime, String time,TAData data, TAManagerApp taApp, TAWorkspace workspace) {
        oldType = orgType;
        newType = type;
        oldDate = orgDate;
        newDate = date;
        oldTitle = orgTitle;
        newTitle = title;
        oldTopic = orgTopic;
        newTopic = topic;
        oldLink = orgLink;
        newLink = link;
        oldCriteria = orgCriteria;
        newCriteria = criteria;
        oldTime = orgTime;
        newTime = time;
        taData = data;
        
        s = data.getSchedule(orgTitle);
        app=taApp; 
        transWorkspace=workspace; 
    }

    @Override
    public void doTransaction() {  //Control Y 
        System.out.println("updateSchedule doTransaction ");
        Schedule schedule = taData.getSchedule(oldTitle);
        schedule.setType(newType);
        schedule.setDate(newDate);
        schedule.setTitle(newTitle);
        schedule.setTopic(newTopic);
        schedule.setLink(newLink);
        schedule.setCriteria(newCriteria);
        schedule.setTime(newLink);
        
        TAController controller = new TAController(app);
        s.setType(newType);
        s.setDate(newDate);
        s.setTitle(newTitle);
        s.setTopic(newTopic);
        s.setLink(newLink);
        s.setCriteria(newCriteria);
        s.setTime(newLink);

        transWorkspace.scheduleTable.refresh();
        
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("updateProject undoTransaction ");
        taData.getSchedule(newTitle).setTitle(oldTitle);
        s.setType(oldType);
        s.setDate(oldDate);
        s.setTitle(oldTitle);
        s.setTopic(oldTopic);
        s.setLink(oldLink);
        s.setCriteria(oldCriteria);
        s.setTime(oldLink);
        
        transWorkspace.scheduleTable.refresh();
        
        //transWorkspace.taTable.refresh();

    }

}
