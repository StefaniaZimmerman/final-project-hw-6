/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.Collections;
import java.util.HashMap;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import tam.data.Project;
import tam.data.TAData;

/**
 *
 * @author khurr
 */
public class AddProject_Transaction implements jTPS_Transaction {

    private String ptitle;
    private String plink;
    private String pcolor;
    private String ptextColor;
    private TAData data;

    public AddProject_Transaction(String title, String color, String textColor, String link, TAData taData) {
        ptitle = title;
        plink = link;
        pcolor = color;
        ptextColor = textColor;
        data = taData;
    }

    @Override
    public void doTransaction() {  //Control Y 
        data.addProject(ptitle,pcolor,ptextColor,plink);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransactionProject");
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo TransactionProject");
        ObservableList<Project> projectList = data.getProjects();
        for (Project p : projectList) {
            if (ptitle.equals(p.getName())) {
                projectList.remove(p);
                return;
            }

        }
        // data.removeTA(taName);

    }

}
