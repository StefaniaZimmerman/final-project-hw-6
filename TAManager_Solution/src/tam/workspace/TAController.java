package tam.workspace;

import djf.controller.AppFileController;
import static djf.settings.AppPropertyType.EXPORT_COMPLETED_MESSAGE;
import static djf.settings.AppPropertyType.EXPORT_COMPLETED_TITLE;
import static djf.settings.AppPropertyType.EXPORT_TITLE;
import static djf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import static tam.TAManagerProp.*;
import djf.ui.AppYesNoDialogSingleton;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import org.apache.commons.io.FileUtils;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.Project;
import tam.data.Recitation;
import tam.data.Schedule;
import tam.data.Student;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.file.TAFiles;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_CELL;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static tam.style.TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import tam.workspace.TAWorkspace;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file toolbar.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class TAController{

    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
    static jTPS jTPS = new jTPS();
    public int remainingActions(){
        return jTPS.getNumberOfTransactions();
    }
    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    //Method for exporting json objects to file
    public void writeFile(String expDirectory,JsonObject jsonObject) throws FileNotFoundException{
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(jsonObject);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(expDirectory);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(jsonObject);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(expDirectory);
	pw.write(prettyPrinted);
	pw.close();
    
    }
    //Helper method for exporting
    public void prepareToExport() throws FileNotFoundException{
        TAData data = (TAData)app.getDataComponent();
        TAFiles files = (TAFiles)app.getFileComponent();
        //Get the export directory
        String expDirectory = data.getExportDirectory();
        //Create five json files
        JsonObject courseInfo = files.getCourseInfoJSON(data);
        JsonObject TAInfo = files.getTAInfoJSON(data);
        JsonObject recitationInfo = files.getRecitationInfoJSON(data);
        JsonObject scheduleInfo = files.getScheduleInfoJSON(data);
        JsonObject projectInfo = files.getProjectInfoJSON(data);
        // WE'LL NEED THIS TO GET CUSTOM STUFF
        //Holds all the files
        File jsonDirectoryFolder = new File(expDirectory+"/public_html/js"); 
        String jsonDirectory =(expDirectory+"/public_html/js");
        writeFile(jsonDirectory,courseInfo);
        writeFile(jsonDirectory,TAInfo);
        writeFile(jsonDirectory,recitationInfo);
        writeFile(jsonDirectory,scheduleInfo);
        writeFile(jsonDirectory,projectInfo);
        
	PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        dialog.show(props.getProperty(EXPORT_COMPLETED_TITLE), props.getProperty(EXPORT_COMPLETED_MESSAGE));

    }
     public static void copy(File src, File des) throws IOException{
         FileUtils.copyDirectory(src,des); 
     }
     public static void copyFile(File src, File des)throws IOException{
         FileUtils.copyFile(src,des); 
     }
    /**
     * This helper method should be called every time an edit happens.
     */
    private void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }

    //This method pulls data to be exported and then sends it to the actutal exporter

    /**
     * This method responds to when the user requests to add a new TA via the
     * UI. Note that it must first do some validation to make sure a unique name
     * and email address has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        EmailValidator checkEmail = new EmailValidator();

        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
        } // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name, email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));
        } // **********Check the TA Email Address for correct format 
        else if (!checkEmail.validate(email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));

        } // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            //data.addTA(name, email);
            jTPS_Transaction transaction1 = new AddTA_Transaction(name, email, data);

            jTPS.addTransaction(transaction1);
            //jTPS.doTransaction();
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");

            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }

    /**
     * This function provides a response for when the user presses a keyboard
     * key. Note that we're only responding to Delete, to remove a TA.
     *
     * @param code The keyboard code pressed.
     */
    public void handleKeyPress(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?

        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView taTable = workspace.getTATable();

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                TeachingAssistant ta = (TeachingAssistant) selectedItem;
                String taName = ta.getName();
                TAData data = (TAData) app.getDataComponent();
                HashMap<String, StringProperty> officeHours = data.getOfficeHours();
                jTPS_Transaction transaction1 = new DeleteTA_Transaction(ta, data, officeHours);
                jTPS.addTransaction(transaction1);
                //Clear out all fields
                workspace.getNameTextField().setText("");
                workspace.getEmailTextField().setText("");
                workspace.recitationList.refresh();
                markWorkAsEdited();
            }
        }

    }
    
    public void deleteTA(){
         // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView taTable = workspace.getTATable();

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                TeachingAssistant ta = (TeachingAssistant) selectedItem;
                String taName = ta.getName();
                TAData data = (TAData) app.getDataComponent();
                HashMap<String, StringProperty> officeHours = data.getOfficeHours();
                jTPS_Transaction transaction1 = new DeleteTA_Transaction(ta, data, officeHours);
                jTPS.addTransaction(transaction1);
                //Clear fields
                
                workspace.getNameTextField().setText("");
                workspace.getEmailTextField().setText("");
                /*data.removeTA(taName);

                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
               
                for (Label label : labels.values()) {
                    if (label.getText().equals(taName)
                            || (label.getText().contains(taName + "\n"))
                            || (label.getText().contains("\n" + taName))) {
                        data.removeTAFromCell(label.textProperty(), taName);
                    }
                } */
                // WE'VE CHANGED STUFF
                workspace.recitationList.refresh();
                markWorkAsEdited();
            }      
    }

    public void handleUndoTransaction() {
        System.out.println("Transaction Control Z");
        jTPS.undoTransaction();
        
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        workspace.recitationList.refresh();
        markWorkAsEdited();
    }

    public void handleReDoTransaction() {
        System.out.println("Transaction crlt y");
        jTPS.doTransaction();
        markWorkAsEdited();
    }

    /**
     * This function provides a response for when the user clicks on the office
     * hours grid to add or remove a TA to a time slot.
     *
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            // GET THE TA
            jTPS_Transaction transaction = new ToggleTa_Transaction(selectedItem, app, pane);
            jTPS.addTransaction(transaction);

            /*TeachingAssistant ta = (TeachingAssistant) selectedItem;
            String taName = ta.getName();
            TAData data = (TAData) app.getDataComponent();
            String cellKey = pane.getId();

            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
            data.toggleTAOfficeHours(cellKey, taName);*/
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }

    public void handleTaClicked(Pane pane, Pane addBox) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            //selectedItem = taTable.getSelectionModel().getSelectedItem();
            TeachingAssistant ta = (TeachingAssistant) selectedItem;
            // workspace.nameTextField.clear();
            //workspace.emailTextField.clear(); 
            System.out.println("TA CLICKED");
            System.out.println(ta.getName());
            // addBox.getChildren().remove(workspace.addButton); 
            // addBox.getChildren().remove(workspace.clearButton1); 
            addBox.getChildren().add(workspace.nameTextField);
            addBox.getChildren().add(workspace.emailTextField);
            addBox.getChildren().add(workspace.updateTaButton);
            addBox.getChildren().add(workspace.clearButton);
            // GET THE TA
            String taName = ta.getName();
            String taEmail = ta.getEmail();
            TAData data = (TAData) app.getDataComponent();

            // SET TextField To TA NAME 
            workspace.nameTextField.setText(taName);
            workspace.emailTextField.setText(taEmail);
            // workspace.updateTaButton.setOnAction(e -> {
            //handleUpdateTA(taName,taEmail,ta);

            // });
            //markWorkAsEdited();   
        }
    }

    public void handleUpdateTA() {
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = (TeachingAssistant) selectedItem;
        String orgName = ta.getName();
        String orgEmail = ta.getEmail();

        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();

        String name = nameTextField.getText();
        String email = emailTextField.getText();
        EmailValidator checkEmail = new EmailValidator();

        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (name.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
        } // **********Check the TA Email Address for correct format 
        else if (!checkEmail.validate(email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));

        } // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            if (!orgName.equalsIgnoreCase(name)) { //case if only name is changed
                jTPS_Transaction transaction2 = new UpdateTA_Transaction(orgName, name, orgEmail, email, data, app, workspace);
                nameTextField.setText(name);
                emailTextField.setText(email);
                jTPS.addTransaction(transaction2);

                //jTPS.doTransaction();
                /*data.getTA(orgName).setName(name);
                handleUpdateTaGrid(orgName, name);
                ta.setName(name);                        // MOVED TO TRANSACTION CASE 
                taTable.refresh();
                 */
                markWorkAsEdited();
            }
            if (!orgEmail.equalsIgnoreCase(email)) {   //case if only email is changed 
                jTPS_Transaction transaction3 = new UpdateTA_EmailOnly_Transaction(orgName, orgEmail, email, data, workspace);
                jTPS.addTransaction(transaction3);

                // data.getTA(orgName).setEmail(email);     //moved to transaction class 
                //ta.setEmail(email);
                markWorkAsEdited();

            }
            if (orgEmail.equalsIgnoreCase(email) && orgName.equalsIgnoreCase(name)) {                //case if nothing is chagned 
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(NO_UPDATE_TITLE), props.getProperty(NO_UPDATE_MESSAGE));

            }
            taTable.refresh();
            nameTextField.setText(name);
            emailTextField.setText(email);

            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            // WE'VE CHANGED STUFF

        }
        // workspace.reloadOfficeHoursGrid(data);

    }

    public void handleTaTableRefresh() {
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        taTable.refresh();
    }

    public void handleUpdateTaGrid(String taName, String newName) {

        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TAData data = (TAData) app.getDataComponent();
        //data.removeTA(taName);

        // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
        HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();

        for (Label label : labels.values()) {   //iterates thourhg the hashmap to find all occurences of orgTA in the office hour grid
            if (label.getText().equals(taName)
                    || (label.getText().contains(taName + "\n"))
                    || (label.getText().contains("\n" + taName))) {
                data.renameTaCell(label.textProperty(), taName, newName);
            }
        }
        TableView taTable = workspace.getTATable();
        Collections.sort(data.getTeachingAssistants());  //sorts the teachingAssistants List 
        taTable.refresh();

        markWorkAsEdited();

    }

    void handleGridCellMouseExited(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData) app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();

        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
    }

    void handleGridCellMouseEntered(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData) app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();

        // THE MOUSED OVER PANE
        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_CELL);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }
    }

    void handleChangeTime(String startTime, String endTime) {
        //TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppYesNoDialogSingleton yesNoDialog = AppYesNoDialogSingleton.getSingleton();
        yesNoDialog.show(props.getProperty(UPDATE_TIME_TITLE), props.getProperty(UPDATE_TIME_MESSAGE));

        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoDialog.getSelection();
        if (selection.equals(AppYesNoDialogSingleton.YES)) {

            int start = convertToMilitaryTime(startTime);
            int end = convertToMilitaryTime(endTime);
            System.out.println(start);

            //TAWorkspace workspace = (TAWorkspace)app.getDataComponent();
            if (start == end || start == -1 || end == -1) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TIME_INPUT_TITLE), props.getProperty(INVALID_TIME_INPUT_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              

            } else if (start > end) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TIME_INPUT_TITLE), props.getProperty(INVALID_TIME_INPUT_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              

            } else if (end < start) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TIME_INPUT_TITLE), props.getProperty(INVALID_TIME_INPUT_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              

            } else {    //At this point the time varialbes are good to go. 
                TAData data = (TAData) app.getDataComponent();

                jTPS_Transaction transaction = new updateTime_Transaction(start, end, data);
                jTPS.addTransaction(transaction);

                //workspace.resetWorkspace(); 
                //workspace.reloadWorkspace(oldData);
                markWorkAsEdited();
                //workspace.reloadOfficeHoursGrid(data);
            }
        }

    }

    public int convertToMilitaryTime(String time) {
        int milTime = 0;
        if (time == null) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              
        } else if (time.equalsIgnoreCase("12:00pm")) {
            milTime = 12;
        } else {
            int index = time.indexOf(":");
            String subStringTime = time.substring(0, index);
            milTime = Integer.parseInt(subStringTime);
            if (time.contains("p")) {
                milTime += 12;
            }
        }
        return milTime;
    }
    //For course info
    public void handleCSSSheetChange(){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TAData data = (TAData) app.getDataComponent();
        data.setStylesheet(workspace.getStylesheetPicker().getValue().toString());
    }
    public void handleUpdateCourseInfo(){
         TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
         TAData data = (TAData) app.getDataComponent();
         if(workspace.getInstructorNameBox().getText().isEmpty()||
            workspace.getInstructorHomeBox().getText().isEmpty()||
            workspace.getCourseTitleBox().getText().isEmpty()){
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(MISSING_COURSE_INFO_TITLE), props.getProperty(MISSING_COURSE_INFO_MESSAGE));
         }
         
         else{
            data.setInstructorName(workspace.getInstructorNameBox().getText());
            data.setInstructorHome(workspace.getInstructorHomeBox().getText());
            data.setCourseTitle(workspace.getCourseTitleBox().getText());
            data.setSubject(workspace.getSubjectBox().getValue().toString());
            data.setCourseNumber(workspace.getNumberBox().getValue().toString());
            data.setSemester(workspace.getSemesterBox().getValue().toString());
            data.setYear(workspace.getYearBox().getValue().toString());
            System.out.println("Course info saved");}
         
         
        
    }
    public void handleExportDirectorySelected(String selectedDirectory, Label label){
         TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
         TAData data = (TAData) app.getDataComponent();
         
         if(selectedDirectory == null){           
                 PropertiesManager props = PropertiesManager.getPropertiesManager();
                 AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                 dialog.show(props.getProperty(MISSING_DIRECTORY_TITLE), props.getProperty(MISSING_DIRECTORY_MESSAGE));
         }
         else{
                label.setText(selectedDirectory);
                data.setExportDirectory(selectedDirectory);
         }
    }
    public void handleTemplateDirectorySelected(String selectedDirectory, Label label){
         TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
         TAData data = (TAData) app.getDataComponent();
         
         if(selectedDirectory == null){           
                 PropertiesManager props = PropertiesManager.getPropertiesManager();
                 AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                 dialog.show(props.getProperty(MISSING_DIRECTORY_TITLE), props.getProperty(MISSING_DIRECTORY_MESSAGE));
         }
         else{
                label.setText(selectedDirectory);
                data.setTemplateDirectory(selectedDirectory);
         }
    }
    public void handleBannerSelected(String selectedDirectory, File file) throws MalformedURLException{
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        if(selectedDirectory == null){
                 PropertiesManager props = PropertiesManager.getPropertiesManager();
                 AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                 dialog.show(props.getProperty(MISSING_IMAGE_TITLE), props.getProperty(MISSING_IMAGE_MESSAGE));
        }
        else{
                    data.setBannerImage(selectedDirectory);
                    String localUrl = file.toURI().toURL().toString();
                    Image image = new Image(localUrl, false);
                    workspace.bannerImageDisplay.setImage(image);
        }
    }
    public void handleLeftFooterSelected(String selectedDirectory, File file) throws MalformedURLException{
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        if(selectedDirectory == null){
                 PropertiesManager props = PropertiesManager.getPropertiesManager();
                 AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                 dialog.show(props.getProperty(MISSING_IMAGE_TITLE), props.getProperty(MISSING_IMAGE_MESSAGE));
        }
        else{
                    data.setLeftFooterImage(selectedDirectory);
                    String localUrl = file.toURI().toURL().toString();
                    Image image = new Image(localUrl, false);
                    workspace.leftFooterImageDisplay.setImage(image);
        }
    }
    public void handleRightFooterSelected(String selectedDirectory, File file) throws MalformedURLException{
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        if(selectedDirectory == null){
                 PropertiesManager props = PropertiesManager.getPropertiesManager();
                 AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                 dialog.show(props.getProperty(MISSING_IMAGE_TITLE), props.getProperty(MISSING_IMAGE_MESSAGE));
        }
        else{
                    data.setRightFooterImage(selectedDirectory);
                    String localUrl = file.toURI().toURL().toString();
                    Image image = new Image(localUrl, false);
                    workspace.rightFooterImageDisplay.setImage(image);
        }
    }
    
    
    
    //Recitation controller
        public void updateTAComboBoxOne(ComboBox combobox){
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TAData data = (TAData) app.getDataComponent();
            //Clear the list
            workspace.TANamesListOne.clear();
            //Add none
            workspace.TANamesListOne.add("None");
            ObservableList<TeachingAssistant> tas = data.getTeachingAssistants();
            for(TeachingAssistant ta : tas){
                workspace.TANamesListOne.add(ta.getName());
            }
        }
        
        public void updateTAComboBoxTwo(ComboBox combobox){
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TAData data = (TAData) app.getDataComponent();
            //Clear the list
            workspace.TANamesListTwo.clear();
            //Add none
            workspace.TANamesListTwo.add("None");
            ObservableList<TeachingAssistant> tas = data.getTeachingAssistants();
            for(TeachingAssistant ta : tas){
                workspace.TANamesListTwo.add(ta.getName());
            }
        }
        //Recitation controller
        public void handleRecitationClicked() {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView recitationTable = workspace.getRecitationList();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            Recitation r = (Recitation) selectedItem;
            /*
            addBox.getChildren().add(workspace.sectionInput);
            addBox.getChildren().add(workspace.locationInput);
            addBox.getChildren().add(workspace.updateButton);
            addBox.getChildren().add(workspace.clearButton);*/
            // GET THE information
            String section = r.getSection();
            String location = r.getLocation();
            String instructor = r.getInstructor();
            String date = r.getDate();
            String TAOne = r.getTAOne();
            String TATwo = r.getTATwo();
            TAData data = (TAData) app.getDataComponent();

            workspace.recitationSectionInput.setText(section);
            workspace.recitationLocationInput.setText(location);
            workspace.recitationInstructorInput.setText(instructor);
            workspace.recitationDateInput.setText(date);
            workspace.recitationTAOneInput.setValue(TAOne);
            workspace.recitationTATwoInput.setValue(TATwo); 
            System.out.println("Recitation click handled");
        }
        
    }
    
    public void handleEditRecitation(){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView recitationTable = workspace.recitationList;
        
        Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();
        Recitation r = (Recitation) selectedItem;
        String orgSection = r.getSection();
        String orgInstructor = r.getInstructor();
        String orgDate = r.getDate();
        String orgLocation = r.getLocation();
        String orgTAOne = r.getTAOne();
        String orgTATwo = r.getTATwo();

        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (workspace.recitationDateInput.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_RECITATION_TIME_TITLE), props.getProperty(MISSING_RECITATION_TIME_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (workspace.recitationSectionInput.getText().isEmpty()||
                workspace.recitationInstructorInput.getText().isEmpty()||
                workspace.recitationLocationInput.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_RECITATION_INFO_TITLE), props.getProperty(MISSING_RECITATION_INFO_MESSAGE));
        } 
        else {
            // ADD THE NEW TA TO THE DATA
            jTPS_Transaction transaction2 = new UpdateRecitation_Transaction(orgSection, workspace.recitationSectionInput.getText(),
                    orgInstructor, workspace.recitationInstructorInput.getText(), 
                    orgDate, workspace.recitationDateInput.getText(),
                    orgLocation, workspace.recitationLocationInput.getText(),
                    orgTAOne,workspace.recitationTAOneInput.getValue().toString(),
                    orgTATwo,workspace.recitationTATwoInput.getValue().toString(),
                    data, app, workspace);
            jTPS.addTransaction(transaction2);
            markWorkAsEdited();

        }
    }
    //For adding recitation
    public void handleAddRecitation(){
              // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();

        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (workspace.recitationDateInput.getText().equals("")) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_RECITATION_TIME_TITLE), props.getProperty(MISSING_RECITATION_TIME_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (workspace.recitationInstructorInput.getText().equals("")||
                workspace.recitationSectionInput.getText().equals("")||
                workspace.recitationLocationInput.getText().equals("")) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_RECITATION_INFO_TITLE), props.getProperty(MISSING_RECITATION_INFO_MESSAGE));
        } // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsRecitation(workspace.recitationDateInput.getText())) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(RECITATION_TIME_NOT_UNIQUE_TITLE), props.getProperty(RECITATION_TIME_NOT_UNIQUE_MESSAGE));
        } // **********Check the TA Email Address for correct format 
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            //data.addTA(name, email);
            //Get the hex values of the colors
            String TAOne;
            String TATwo;
            if(workspace.recitationTAOneInput.getValue()!=null){
                TAOne = workspace.recitationTAOneInput.getValue().toString();
            }
            else{
                TAOne = "";
            }
            if(workspace.recitationTATwoInput.getValue()!=null){
                
                 TATwo = workspace.recitationTATwoInput.getValue().toString();
            }
            else{
                 TATwo ="";
            }
            
            jTPS_Transaction transaction1 = new AddRecitation_Transaction(workspace.recitationSectionInput.getText(),
                    workspace.recitationInstructorInput.getText(),workspace.recitationDateInput.getText(),
                    workspace.recitationLocationInput.getText(),TAOne,TATwo,data);

            jTPS.addTransaction(transaction1);
            //jTPS.doTransaction();
            // CLEAR THE TEXT FIELDS
            workspace.recitationSectionInput.setText("");
            workspace.recitationInstructorInput.setText("");
            workspace.recitationDateInput.setText("");
            workspace.recitationLocationInput.setText("");

            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            workspace.recitationSectionInput.requestFocus();
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
  
    }
    //For removing recitation
    public void deleteRecitation(){
            // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView recitationTable = workspace.recitationList;

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Recitation r = (Recitation) selectedItem;
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteRecitation_Transaction(r, data);
                jTPS.addTransaction(transaction1);
                //Clear fields
                
        workspace.recitationSectionInput.setText("");
        workspace.recitationInstructorInput.setText("");
        workspace.recitationDateInput.setText("");
        workspace.recitationLocationInput.setText("");
        workspace.recitationTAOneInput.setValue("");
        workspace.recitationTATwoInput.setValue("");
                System.out.println("Recitation deleted");
                markWorkAsEdited();
            }      
    }
     public void handleKeyPressRecitation(KeyCode code){
          // DID THE USER PRESS THE DELETE KEY?

        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            System.out.println("Delete key pressed");
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView recitationTable = workspace.recitationList;

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Recitation r = (Recitation) selectedItem;
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteRecitation_Transaction(r, data);
                jTPS.addTransaction(transaction1);
                //clear fields
                
        workspace.recitationSectionInput.setText("");
        workspace.recitationInstructorInput.setText("");
        workspace.recitationDateInput.setText("");
        workspace.recitationLocationInput.setText("");
        workspace.recitationTAOneInput.setValue("");
        workspace.recitationTATwoInput.setValue("");
                markWorkAsEdited();
                System.out.println("Deleted with key");
            }
        }
     }
  

        
        
    //Schedule controller
    public void handleUpdateDates(){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView scheduleTable = workspace.scheduleTable;
        TAData data = (TAData) app.getDataComponent();
        //Stuff to get original dates
        String orgStartingDate = data.getStartingDate();
        String orgEndingDate = data.getEndingDate();
        //Stuff to get new dates
            LocalDate dateTempStarting = workspace.startingDateInput.getValue();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            formatter = formatter.withLocale( Locale.US ); 
        String startingDate = formatter.format(dateTempStarting);
            LocalDate dateTempEnding = workspace.endingDateInput.getValue();
        String endingDate = formatter.format(dateTempEnding);
        //Make sure that the starting date is before the ending date
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (dateTempStarting.isAfter(dateTempEnding)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_DATES_TITLE), props.getProperty(INVALID_DATES_MESSAGE));
            
        } 
        else{
            jTPS_Transaction transaction2 = new UpdateDateBoundries_Transaction(orgStartingDate,startingDate,orgEndingDate,endingDate,data, app, workspace);
            jTPS.addTransaction(transaction2);}
    }
    public void handleScheduleClicked() {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView scheduleTable = workspace.scheduleTable;

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            Schedule s = (Schedule) selectedItem;

            String type = s.getType();
            String time = s.getTime();
            String topic = s.getTopic();
            String criteria = s.getCriteria();
            String date = s.getDate();
            String title = s.getTitle();
            String link = s.getLink();
            
            TAData data = (TAData) app.getDataComponent();

            workspace.scheduleTypeInput.setValue(type);
            workspace.scheduleTimeInput.setText(time);
            workspace.scheduleTopicInput.setText(topic);
            workspace.scheduleCriteriaInput.setText(criteria);
            //For the date
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            formatter = formatter.withLocale( Locale.US ); 
            LocalDate dateTemp = LocalDate.parse(date, formatter);
            workspace.scheduleDateInput.setValue(dateTemp);
            workspace.scheduleTitleInput.setText(title); 
            workspace.scheduleLinkInput.setText(link);
            System.out.println("Schedule click handled");
        }
    }
    //Schedule edit
        public void handleEditSchedule(){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView scheduleTable = workspace.scheduleTable;
        Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
        Schedule s = (Schedule) selectedItem;
        
        String orgType = s.getType();
        String orgDate = s.getDate();
        String orgTitle = s.getTitle();
        String orgTopic = s.getTopic();
        String orgLink = s.getLink();
        String orgCriteria = s.getCriteria();
        String orgTime = s.getTime();

        String type = workspace.scheduleTypeInput.getValue().toString();
        //Stuff to get date
            LocalDate dateTemp = workspace.scheduleDateInput.getValue();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            formatter = formatter.withLocale( Locale.US ); 
            
            
        String date = formatter.format(dateTemp);
        String title = workspace.scheduleTitleInput.getText();
        String topic = workspace.scheduleTopicInput.getText();
        String link = workspace.scheduleLinkInput.getText();
        String criteria = workspace.scheduleCriteriaInput.getText();
        String time = workspace.scheduleTimeInput.getText();

        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (workspace.scheduleTitleInput.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SCHEDULE_TITLE), props.getProperty(MISSING_SCHEDULE_TITLE_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (date.equals("")||time.equals("")) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SCHEDULE_INFORMATION_TITLE), props.getProperty(MISSING_SCHEDULE_INFORMATION_MESSAGE));
        } 
        else {
            // ADD THE NEW TA TO THE DATA
            jTPS_Transaction transaction2 = new UpdateSchedule_Transaction(orgType, type, orgDate, date, orgTitle, title, orgTopic, topic,
                    orgLink,link,orgCriteria,criteria,orgTime,time,data, app, workspace);
            jTPS.addTransaction(transaction2);
            markWorkAsEdited();

        }
        
    }
        //Schedule add
            public void handleAddSchedule(){
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();

        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (workspace.scheduleTitleInput.getText().equals("")) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SCHEDULE_TITLE), props.getProperty(MISSING_SCHEDULE_TITLE_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (workspace.scheduleTimeInput.getText().equals("")||workspace.scheduleDateInput.getValue().equals(null)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SCHEDULE_INFORMATION_TITLE), props.getProperty(MISSING_SCHEDULE_INFORMATION_MESSAGE));
        } // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsSchedule(workspace.scheduleTitleInput.getText())) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(SCHEDULE_NOT_UNIQUE_TITLE), props.getProperty(SCHEDULE_NOT_UNIQUE_MESSAGE));
        } // **********Check the TA Email Address for correct format 
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            //data.addTA(name, email);
            //Get the hex values of the colors
            String type = workspace.scheduleTypeInput.getValue().toString();
                    //Stuff to get date
            LocalDate dateTemp = workspace.scheduleDateInput.getValue();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            formatter = formatter.withLocale( Locale.US ); 
            
            
            String date = formatter.format(dateTemp);
            //String  String initLink, String initCriteria, String initTime
            
            
            jTPS_Transaction transaction1 = new AddSchedule_Transaction(type, date, workspace.scheduleTitleInput.getText(),
                    workspace.scheduleTopicInput.getText(),workspace.scheduleLinkInput.getText(),workspace.scheduleCriteriaInput.getText(),
                    workspace.scheduleTimeInput.getText(),data);

            jTPS.addTransaction(transaction1);
            //jTPS.doTransaction();
            // CLEAR THE TEXT FIELDS
            workspace.scheduleDateInput.getEditor().clear();
            workspace.scheduleTitleInput.setText("");
            workspace.scheduleTopicInput.setText("");
            workspace.scheduleLinkInput.setText("");
            workspace.scheduleCriteriaInput.setText("");
            workspace.scheduleTimeInput.setText("");
            workspace.scheduleTitleInput.requestFocus();
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
            
    }
    }
            //Schedule remove
     public void deleteSchedule(){
         // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView scheduleTable = workspace.scheduleTable;

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Schedule s = (Schedule) selectedItem;
                String scheduleTitle = s.getTitle();
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteSchedule_Transaction(s, data);
                jTPS.addTransaction(transaction1);
                //clear fields
                
        workspace.scheduleTypeInput.setValue("");
        workspace.scheduleDateInput.getEditor().clear();
        workspace.scheduleTitleInput.setText("");
        workspace.scheduleTimeInput.setText("");
        //topic link and criteria
        workspace.scheduleTopicInput.setText("");
        workspace.scheduleLinkInput.setText("");
        workspace.scheduleCriteriaInput.setText("");
                System.out.println("Schedule deleted");
                markWorkAsEdited();
            }      
    }
     public void handleKeyPressSchedule(KeyCode code){
          // DID THE USER PRESS THE DELETE KEY?

        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            System.out.println("Delete key pressed");
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView scheduleTable = workspace.scheduleTable;

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Schedule s = (Schedule) selectedItem;
                String name = s.getTitle();
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteSchedule_Transaction(s, data);
                jTPS.addTransaction(transaction1);
                //Clear fields
                
        workspace.scheduleTypeInput.setValue("");
        workspace.scheduleDateInput.getEditor().clear();
        workspace.scheduleTitleInput.setText("");
        workspace.scheduleTimeInput.setText("");
        //topic link and criteria
        workspace.scheduleTopicInput.setText("");
        workspace.scheduleLinkInput.setText("");
        workspace.scheduleCriteriaInput.setText("");
                markWorkAsEdited();
                System.out.println("Deleted with key");
            }
        }
     }

        
    
    
    //Project Controller
    public void updateTeamsComboBox(ComboBox combobox){
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TAData data = (TAData) app.getDataComponent();
            //Clear the list
            workspace.teamsNamesList.clear();
            ObservableList<Project> projects = data.getProjects();
            for(Project p : projects){
                workspace.teamsNamesList.add(p.getName());
            }
        }
    public void handleEditProject(){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView projectTable = workspace.projectTable;
        Object selectedItem = projectTable.getSelectionModel().getSelectedItem();
        Project p = (Project) selectedItem;
        String orgName = p.getName();
        String orgColor = p.getColor();
        String orgTextColor = p.getTextColor();
        String orgLink = p.getLink();

        String name = workspace.projectNameInput.getText();
        String link = workspace.projectLink.getText();
        String color = toRGBCode(workspace.projectColorInput.getValue());
        String textColor = toRGBCode(workspace.projectTextColorInput.getValue());

        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (workspace.projectNameInput.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_PROJECT_TITLE), props.getProperty(MISSING_PROJECT_TITLE_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (workspace.projectLink.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_PROJECT_LINK_TITLE), props.getProperty(MISSING_PROJECT_LINK_MESSAGE));
        } 
        else {
            // ADD THE NEW TA TO THE DATA
            jTPS_Transaction transaction2 = new UpdateProject_Transaction(orgName, name, orgColor, color, orgTextColor, textColor, orgLink, link, data, app, workspace);
            jTPS.addTransaction(transaction2);
            markWorkAsEdited();

        }
        
    }
    public void handleProjectClicked() {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView projectTable = workspace.getProjectTable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = projectTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            Project p = (Project) selectedItem;

            String name = p.getName();
            String color = p.getColor();
            String textColor = p.getTextColor();
            String link = p.getLink();
            
            TAData data = (TAData) app.getDataComponent();

            workspace.projectNameInput.setText(name);
            workspace.projectColorInput.setValue(Color.web(color));
            workspace.projectTextColorInput.setValue(Color.web(textColor));
            workspace.projectLink.setText(link);
            System.out.println("Project click handled");
        }
    }
    public void handleAddProject(){
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();

        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (workspace.projectNameInput.getText().equals("")) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_PROJECT_TITLE), props.getProperty(MISSING_PROJECT_TITLE_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (workspace.projectLink.getText().equals("")) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_PROJECT_LINK_TITLE), props.getProperty(MISSING_PROJECT_LINK_MESSAGE));
        } // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsProject(workspace.projectNameInput.getText(), workspace.projectLink.getText())) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(PROJECT_TITLE_AND_LINK_NOT_UNIQUE_TITLE), props.getProperty(PROJECT_TITLE_AND_LINK_NOT_UNIQUE_MESSAGE));
        } // **********Check the TA Email Address for correct format 
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            //data.addTA(name, email);
            //Get the hex values of the colors
            String color = toRGBCode(workspace.projectColorInput.getValue());
            String textColor = toRGBCode(workspace.projectTextColorInput.getValue());
            
            jTPS_Transaction transaction1 = new AddProject_Transaction(workspace.projectNameInput.getText(),color, textColor, workspace.projectLink.getText(),data);

            jTPS.addTransaction(transaction1);
            //jTPS.doTransaction();
            // CLEAR THE TEXT FIELDS
            workspace.projectNameInput.setText("");
            workspace.projectLink.setText("");
            workspace.projectColorInput.setValue(Color.web("WHITE"));
            workspace.projectTextColorInput.setValue(Color.web("WHITE"));

            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            workspace.projectNameInput.requestFocus();
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }
    public void deleteProject(){
         // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView projectTable = workspace.projectTable;

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = projectTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Project p = (Project) selectedItem;
                String projectTitle = p.getName();
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteProject_Transaction(p, data,workspace);
                jTPS.addTransaction(transaction1);
                //Clear fields
                
        workspace.projectColorInput.setValue(Color.WHITE);
        workspace.projectTextColorInput.setValue(Color.WHITE);
        workspace.projectNameInput.setText("");
        workspace.projectLink.setText("");
                System.out.println("Project deleted");
                markWorkAsEdited();
            }      
    }
    public void handleKeyPressProject(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?

        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            System.out.println("Delete key pressed");
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView projectTable = workspace.projectTable;

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = projectTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Project p = (Project) selectedItem;
                String name = p.getName();
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteProject_Transaction(p, data,workspace);
                jTPS.addTransaction(transaction1);
                //Clear fields
                
        workspace.projectColorInput.setValue(Color.WHITE);
        workspace.projectTextColorInput.setValue(Color.WHITE);
        workspace.projectNameInput.setText("");
        workspace.projectLink.setText("");
                markWorkAsEdited();
                System.out.println("Deleted with key");
            }
        }

    }
    

    //Helper method to convert color to HEX
    public static String toRGBCode( Color color )
    {
        return String.format( "%02X%02X%02X",
            (int)( color.getRed() * 255 ),
            (int)( color.getGreen() * 255 ),
            (int)( color.getBlue() * 255 ) );
    }
    public void handleStudentClicked() {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView studentTable = workspace.getStudentTable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = studentTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            Student s = (Student) selectedItem;

            String firstName = s.getFirstName();
            String lastName = s.getLastName();
            String team = s.getTeams();
            String role = s.getRole();
            
            TAData data = (TAData) app.getDataComponent();
            
            workspace.studentFirstNameInput.setText(firstName);
            workspace.studentLastNameInput.setText(lastName);
            workspace.studentTeamInput.setValue(team);
            workspace.studentRoleInput.setText(role);
            
            System.out.println("Student click handled");
        }
    }
    public void handleEditStudent(){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView projectTable = workspace.studentTable;
        Object selectedItem = projectTable.getSelectionModel().getSelectedItem();
        Student s = (Student) selectedItem;
        String orgFirstName = s.getFirstName();
        String orgLastName = s.getLastName();
        String orgTeam = s.getTeams();
        String orgRole = s.getRole();

        String firstName = workspace.studentFirstNameInput.getText();
        String lastName = workspace.studentLastNameInput.getText();
        String team = workspace.studentTeamInput.getValue().toString();
        String role = workspace.studentRoleInput.getText();

        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (workspace.studentFirstNameInput.getText().isEmpty()||workspace.studentLastNameInput.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_STUDENT_NAME_TITLE), props.getProperty(MISSING_STUDENT_NAME_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (workspace.studentRoleInput.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_STUDENT_ROLE_TITLE), props.getProperty(MISSING_STUDENT_ROLE_MESSAGE));
        } 
        else {
            // ADD THE NEW TA TO THE DATA
            jTPS_Transaction transaction2 = new UpdateStudent_Transaction(orgFirstName,firstName,orgLastName,lastName,orgTeam,team,orgRole,role, data, app, workspace);
            jTPS.addTransaction(transaction2);
            markWorkAsEdited();
        }
    }
        
        //adding a student
    public void handleAddStudent(){
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();

        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        String fullName = workspace.studentFirstNameInput.getText()+workspace.studentLastNameInput.getText();
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (workspace.studentFirstNameInput.getText().equals("")||workspace.studentLastNameInput.getText().equals("")) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_STUDENT_NAME_TITLE), props.getProperty(MISSING_STUDENT_NAME_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (workspace.studentRoleInput.getText().equals("")) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_STUDENT_ROLE_TITLE), props.getProperty(MISSING_STUDENT_ROLE_MESSAGE));
        } // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsStudent(fullName)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(STUDENT_FIRST_NAME_NOT_UNIQUE_TITLE), props.getProperty(STUDENT_FIRST_NAME_NOT_UNIQUE_MESSAGE));
        } // **********Check the TA Email Address for correct format 
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            //data.addTA(name, email);
            //Get the hex values of the colors
            String team = workspace.studentTeamInput.getValue().toString();
            
            jTPS_Transaction transaction1 = new AddStudent_Transaction(workspace.studentFirstNameInput.getText(),
                                                                        workspace.studentLastNameInput.getText(),
                                                                        workspace.studentRoleInput.getText(),team,data);

            jTPS.addTransaction(transaction1);
            //jTPS.doTransaction();
            // CLEAR THE TEXT FIELDS
            workspace.studentFirstNameInput.setText("");
            workspace.studentLastNameInput.setText("");
            workspace.studentRoleInput.setText("");
            workspace.studentFirstNameInput.requestFocus();
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
            
    }
    }
    public void deleteStudent(){
                 // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView projectTable = workspace.projectTable;

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = workspace.studentTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Student s = (Student) selectedItem;
                String studentName = s.getFirstName();
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteStudent_Transaction(s, data);
                jTPS.addTransaction(transaction1);
                //Clear fields
                
        workspace.studentFirstNameInput.setText("");
        workspace.studentLastNameInput.setText("");
        workspace.studentRoleInput.setText("");
        workspace.studentTeamInput.setValue("");
                System.out.println("Student deleted");
                markWorkAsEdited();
            }      
    }
        public void handleKeyPressStudent(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?

        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            System.out.println("Delete key pressed");
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView studentTable = workspace.studentTable;

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Student s = (Student) selectedItem;
                String name = s.getFirstName();
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteStudent_Transaction(s, data);
                jTPS.addTransaction(transaction1);
                markWorkAsEdited();
                //clear fields
                
        workspace.studentFirstNameInput.setText("");
        workspace.studentLastNameInput.setText("");
        workspace.studentRoleInput.setText("");
        workspace.studentTeamInput.setValue("");
                System.out.println("Deleted with key");
            }
        }

    }
    
        
}