/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.Collections;
import java.util.HashMap;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import tam.data.Project;
import tam.data.Schedule;
import tam.data.Student;
import tam.data.TAData;

/**
 *
 * @author khurr
 */
public class AddSchedule_Transaction implements jTPS_Transaction {

    private String sType;
    private String sDate;
    private String sTitle;
    private String sTopic;
    private String sLink;
    private String sCriteria;
    private String sTime;
    private TAData data;

    public AddSchedule_Transaction(String type, String date, String title, String topic, String link, String criteria, String time, TAData taData) {
        sType = type;
        sDate = date;
        sTitle = title;
        sTopic = topic;
        sLink = link;
        sCriteria = criteria;
        sTime = time;
        data = taData;
    }

    @Override
    public void doTransaction() {  //Control Y 
        data.addSchedule(sType,sDate,sTitle,sTopic,sLink,sCriteria,sTime);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransactionSchedule");
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo TransactionSchedule");
        ObservableList<Schedule> scheduleList = data.getSchedules();
        for (Schedule s : scheduleList) {
            if (sTitle.equals(s.getTitle())) {
                scheduleList.remove(s);
                return;
            }

        }
        // data.removeTA(taName);

    }

}
