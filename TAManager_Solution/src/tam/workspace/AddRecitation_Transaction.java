/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.Collections;
import java.util.HashMap;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import tam.data.Recitation;
import tam.data.Student;
import tam.data.TAData;

/**
 *
 * @author khurr
 */
public class AddRecitation_Transaction implements jTPS_Transaction {

    private String rSection;
    private String rInstructor;
    private String rDate;
    private String rLocation;
    private String rTAOne;
    private String rTATwo;
    private TAData data;

    public AddRecitation_Transaction(String section, String instructor, String date, 
            String location, String TAOne, String TATwo, TAData taData) {
        rSection = section;
        rInstructor = instructor;
        rDate = date;
        rLocation = location;
        rTAOne = TAOne;
        rTATwo = TATwo;
        data = taData;
    }

    @Override
    public void doTransaction() {  //Control Y 
        data.addRecitation(rSection,rInstructor,rDate,rLocation,rTAOne,rTATwo);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransactionRecitation");
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo TransactionReciation");
        ObservableList<Recitation> recitationList = data.getRecitations();
        for (Recitation r : recitationList) {
            if (rDate.equals(r.getDate())) {
                recitationList.remove(r);
                return;
            }

        }
        // data.removeTA(taName);

    }

}
