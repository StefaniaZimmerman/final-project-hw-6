/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.data.Recitation;
/**
 *
 * @author khurr
 */
public class DeleteTA_Transaction implements jTPS_Transaction {

    private TeachingAssistant ta;
    private TAData data;
    private ArrayList<String> keyArray;
    private final HashMap<String, StringProperty> officeHours;
    private ArrayList<String> recitationsTAOne;
    private ArrayList<String> recitationsTATwo;

    public DeleteTA_Transaction(TeachingAssistant ta, TAData data, HashMap<String, StringProperty> officeHours) {
        
        keyArray = new ArrayList<String>();
        this.data = data;
        this.officeHours = officeHours;
        this.ta = ta; 
        recitationsTAOne = new ArrayList<String>();
        recitationsTATwo = new ArrayList<String>();
        //Find all the recitations where the TA is TAOne
        ObservableList<Recitation> recitations = data.getRecitations();
        for(Recitation r : recitations){
            if(r.getTAOne().equals(ta.getName())){
                recitationsTAOne.add(r.getDate());
                System.out.println("Date added to recitations TAOne list");
            }
            if(r.getTATwo().equals(ta.getName())){
                recitationsTATwo.add(r.getDate());
                System.out.println("Date added to recitations TATwo list");
            }
        }
        
        //Find all the recitations where the TA is TATwo  
    }

    @Override
    public void doTransaction() {  //control Y 
        keyArray.clear(); //clear anystored values 
        String taName = ta.getName();
        data.removeTA(taName);
        //Remove the TA from all recitations
        ObservableList<Recitation> recitations = data.getRecitations();
        for(Recitation r : recitations){
            if(r.getTAOne().equals(ta.getName())){
                r.setTAOne("None");
            }
            else if(r.getTATwo().equals(ta.getName())){
                r.setTATwo("None");
            }
        }
        System.out.println("TADelete Transaction");
        for (HashMap.Entry<String, StringProperty> entry : data.getOfficeHours().entrySet()) {
            String key = entry.getKey();
            StringProperty prop = data.getOfficeHours().get(key);
            if (prop.getValue().equals(taName)
                    || (prop.getValue().contains(taName + "\n"))
                    || (prop.getValue().contains("\n" + taName))) {
                System.out.println(prop.getValue());
                keyArray.add(key);
                data.removeTAFromCell(prop, taName);
            }
            
        }

}

    @Override
    public void undoTransaction() {
        data.addTA(ta.getName(), ta.getEmail());

        for (String key : keyArray) {
            StringProperty prop = officeHours.get(key);
            String cellText = prop.getValue();
            prop.setValue(cellText + "\n" + ta.getName());
        }
        //Restore the TA to any recitations
        for(String s : recitationsTAOne){
            System.out.println("The date is:" + s);
            data.getRecitation(s).setTAOne(ta.getName());
        }
        for(String s : recitationsTATwo){
            System.out.println("The date is:" + s);
            data.getRecitation(s).setTATwo(ta.getName());
 
        }

    }
}
