/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.Collections;
import java.util.HashMap;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import tam.data.Project;
import tam.data.Student;
import tam.data.TAData;

/**
 *
 * @author khurr
 */
public class AddStudent_Transaction implements jTPS_Transaction {

    private String sFirstName;
    private String sLastName;
    private String sRole;
    private String sTeam;
    private String sFullName;
    private TAData data;
    public AddStudent_Transaction(String firstName, String lastName, String role, String team, TAData taData) {
        sFirstName = firstName;
        sLastName = lastName;
        sRole = role;
        sTeam = team;
        data = taData;
        sFullName = firstName+lastName;
    }

    @Override
    public void doTransaction() {  //Control Y 
        data.addStudent(sFirstName,sLastName,sTeam,sRole);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransactionStudent");
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo TransactionStudent");
        ObservableList<Student> studentList = data.getStudents();
        for (Student s : studentList) {
            if (sFullName.equals(s.getFullName())) {
                studentList.remove(s);
                return;
            }

        }
        // data.removeTA(taName);

    }

}
