/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import tam.data.Project;
import tam.data.TAData;
import tam.data.Student;

/**
 *
 * @author khurr
 */
public class DeleteProject_Transaction implements jTPS_Transaction {

    private Project p;
    private TAData data;
    private ArrayList<String> keyArray;
    private ObservableList<Student> deletedStudents = FXCollections.observableArrayList();
    private TAWorkspace transWorkspace;
            
    public DeleteProject_Transaction(Project p, TAData data, TAWorkspace workspace) {
        keyArray = new ArrayList<String>();
        this.data = data;
        this.p = p; 
        transWorkspace=workspace; 
        
    }

    @Override
    public void doTransaction() {  //control Y 
        keyArray.clear(); //clear anystored values 
        String title = p.getName();
        data.removeProject(title);
        //Go through the list of students and delete all students in the team
        //Get the students
        ObservableList<Student> students = data.getStudents();
        for(Student s : students){
            if(s.getTeams().equals(title)){
                //add the student to the deleted list
                deletedStudents.add(s);
                System.out.println(s.getFirstName());
            }
            //add the student to the keep student list
        }
        for(Student s : deletedStudents){
            students.remove(s);
        }
        transWorkspace.studentTable.refresh();
    }

    @Override
    public void undoTransaction() {
        data.addProject(p.getName(),p.getColor(),p.getTextColor(), p.getLink());
        for(Student s : deletedStudents){
            data.getStudents().add(s);
        }
    }
}
