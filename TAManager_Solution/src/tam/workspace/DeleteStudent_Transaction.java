/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import jtps.jTPS_Transaction;
import tam.data.Project;
import tam.data.Student;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class DeleteStudent_Transaction implements jTPS_Transaction {

    private Student s;
    private TAData data;
    private ArrayList<String> keyArray;

    public DeleteStudent_Transaction(Student s, TAData data) {
        keyArray = new ArrayList<String>();
        this.data = data;
        this.s = s; 

    }

    @Override
    public void doTransaction() {  //control Y 
        keyArray.clear(); //clear anystored values 
        String name = s.getFullName();
        data.removeStudent(name);
        System.out.println("Student removed");
    }

    @Override
    public void undoTransaction() {
        data.addStudent(s.getFirstName(),s.getLastName(),s.getTeams(), s.getRole());
    }
}
