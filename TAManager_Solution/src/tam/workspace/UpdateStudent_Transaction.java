/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;


import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam.data.Project;
import tam.data.Student;
import tam.data.TAData;
import tam.data.TeachingAssistant;


/**
 *
 * @author khurr
 */
public class UpdateStudent_Transaction implements jTPS_Transaction {
            
    private String oldFirstName;
    private String newFirstName;
    private String oldLastName;
    private String newLastName;
    private String oldTeam;
    private String newTeam;
    private String oldRole;
    private String newRole;
    private TAData taData;
    private Student s;
    private TAManagerApp app; 
    private TAWorkspace transWorkspace; 

    public UpdateStudent_Transaction(String orgFirstName, String firstName, String orgLastName, String lastName, String orgTeam, String team, String orgRole, String role, TAData data, TAManagerApp taApp, TAWorkspace workspace) {
        oldFirstName = orgFirstName;
        newFirstName = firstName;
        oldLastName = orgLastName;
        newLastName = lastName;
        oldTeam = orgTeam;
        newTeam = team;
        oldRole = orgRole;
        newRole = role;
        taData = data;
        s = data.getStudent(orgFirstName);
        app=taApp; 
        transWorkspace=workspace; 
    }

    @Override
    public void doTransaction() {  //Control Y 
        System.out.println("updateStudent doTransaction ");
        Student student = taData.getStudent(oldFirstName);
        student.setFirstName(newFirstName);
        student.setLastName(newLastName);
        student.setTeam(newTeam);
        student.setRole(newRole);
        student.setFullName(newFirstName,newLastName);
        
        TAController controller = new TAController(app);
        s.setFirstName(newFirstName);                        // MOVED TO TRANSACTION CASE 
        s.setLastName(newLastName);
        s.setTeam(newTeam);
        s.setRole(newRole);
        s.setFullName(newFirstName,newLastName);

        transWorkspace.studentTable.refresh();
        
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("updateProject undoTransaction ");
        taData.getStudent(newFirstName).setFirstName(oldFirstName);
        
        s.setFirstName(oldFirstName);                        // MOVED TO TRANSACTION CASE 
        s.setLastName(oldLastName);
        s.setTeam(oldTeam);
        s.setRole(oldRole);
        s.setFullName(oldFirstName, oldLastName);
        transWorkspace.studentTable.refresh();
        
        //transWorkspace.taTable.refresh();

    }

}
