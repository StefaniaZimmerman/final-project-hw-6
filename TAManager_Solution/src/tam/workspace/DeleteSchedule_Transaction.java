/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import jtps.jTPS_Transaction;
import tam.data.Project;
import tam.data.Schedule;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class DeleteSchedule_Transaction implements jTPS_Transaction {

    private Schedule s;
    private TAData data;
    private ArrayList<String> keyArray;

    public DeleteSchedule_Transaction(Schedule s, TAData data) {
        keyArray = new ArrayList<String>();
        this.data = data;
        this.s = s; 

    }

    @Override
    public void doTransaction() {  //control Y 
        keyArray.clear(); //clear anystored values 
        String title = s.getTitle();
        data.removeSchedule(title);
    }

    @Override
    public void undoTransaction() {
        
        data.addSchedule(s.getType(),s.getDate(),s.getTitle(), s.getTopic(),s.getLink(),s.getCriteria(),s.getTime());
    }
}
