package tam.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.ui.AppGUI;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import tam.TAManagerApp;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import tam.TAManagerProp;
import tam.data.Project;
import tam.data.Recitation;
import tam.data.Schedule;
import tam.data.Student;
import tam.style.TAStyle;
import tam.data.TAData;
import tam.data.TeachingAssistant;
/**
 * This class serves as the workspace component for the TA Manager application.
 * It provides all the user interface controls in the workspace area.
 *
 * @author Richard McKenna
 */
public class TAWorkspace extends AppWorkspaceComponent {
 
    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    TAManagerApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    TAController controller;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    // FOR THE HEADER ON THE LEFT
    HBox tasHeaderBox;
    Label tasHeaderLabel;

    // FOR THE TA TABLE
    TableView<TeachingAssistant> taTable;
    TableColumn<TeachingAssistant, String> nameColumn;
    TableColumn<TeachingAssistant, String> emailColumn;

    // THE TA INPUT
    HBox addBox;
    TextField nameTextField;
    TextField emailTextField;
    Button addButton;
    Button updateTaButton;
    Button clearButton;
    Button clearButton1;

    // THE HEADER ON THE RIGHT
    HBox officeHoursHeaderBox;
    Label officeHoursHeaderLabel;
    //Start and End Time for Office Hours 

    ComboBox newStartTime;
    ComboBox newEndTime;
    Button changeTimeButton;
    


    // THE OFFICE HOURS GRID
    GridPane officeHoursGridPane;
    HashMap<String, Pane> officeHoursGridTimeHeaderPanes;
    HashMap<String, Label> officeHoursGridTimeHeaderLabels;
    HashMap<String, Pane> officeHoursGridDayHeaderPanes;
    HashMap<String, Label> officeHoursGridDayHeaderLabels;
    HashMap<String, Pane> officeHoursGridTimeCellPanes;
    HashMap<String, Label> officeHoursGridTimeCellLabels;
    HashMap<String, Pane> officeHoursGridTACellPanes;
    HashMap<String, Label> officeHoursGridTACellLabels;
    
    //Course info stuff
    public ImageView bannerImageDisplay;
    public ImageView leftFooterImageDisplay;
    public ImageView rightFooterImageDisplay;
    public ComboBox numberPicker;
    public ComboBox yearPicker;
    public ComboBox semesterPicker;
    public ComboBox subjectPicker;
    public TextField courseTitleInput;
    public TextField nameInput;
    public TextField homeInput;
    public Label exportDirPlaceholder;
    public Label templateDirPlaceholder;
    public TableView sitePagesList;
    public Label bannerImagePlaceholder;
    public Label leftFooterImagePlaceholder;
    public Label rightFooterImagePlaceholder;
    public ComboBox stylesheetPicker;
   //Recitation stuff

    TableView recitationList;
    public TextField recitationSectionInput;
    public TextField recitationLocationInput;
    public TextField recitationInstructorInput;
    public TextField recitationDateInput;
    public ComboBox recitationTAOneInput;
    public ComboBox recitationTATwoInput;
    //List of the TA names for comboBox
    ObservableList TANamesListOne;
    ObservableList TANamesListTwo;
    //Schedule stuff
    public DatePicker endingDateInput;
    public DatePicker startingDateInput;
    public DatePicker scheduleDateInput;
    public ComboBox scheduleTypeInput;
    public TextField scheduleTimeInput;
    public TextField scheduleTopicInput;
    public TextField scheduleCriteriaInput;
    public TextField scheduleTitleInput;
    public TextField scheduleLinkInput;
    TableView scheduleTable;
    //Project stuff
    public TextField projectNameInput;
    public TextField projectLink;
    TableView projectTable;
    public ColorPicker projectColorInput;
    public ColorPicker projectTextColorInput;
    //Student stuff (under project)
    public TextField studentFirstNameInput;
    public TextField studentLastNameInput;
    public ComboBox studentTeamInput;
    public TextField studentRoleInput;
    TableView studentTable;
    //Used to generate value for combobox
    ObservableList teamsNamesList;



    /**
     * The contstructor initializes the user interface, except for the full
     * office hours grid, since it doesn't yet know what the hours will be until
     * a file is loaded or a new one is created.
     */
    public TAWorkspace(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //Undo and redo buttons
        //For undo and redo buttons
        AppGUI gui = app.getGUI();
        Button undo = gui.getUndoButton();
        undo.setOnAction(e -> {
            controller.handleUndoTransaction();
        });
        Button redo = gui.getRedoButton();
        redo.setOnAction(e -> {
            controller.handleReDoTransaction();
        });
        
        Button export = gui.getExportButton();
        export.setOnAction(e -> {
            try {
                controller.prepareToExport();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(TAWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        // INIT THE HEADER ON THE LEFT
        tasHeaderBox = new HBox();
        String tasHeaderText = props.getProperty(TAManagerProp.TAS_HEADER_TEXT.toString());
        tasHeaderLabel = new Label(tasHeaderText);
        Button removeTA = new Button("-");
        tasHeaderBox.getChildren().add(tasHeaderLabel);
        tasHeaderBox.getChildren().add(removeTA);
        removeTA.setAlignment(Pos.CENTER);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        taTable = new TableView();
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TAData data = (TAData) app.getDataComponent();
        ObservableList<TeachingAssistant> tableData = data.getTeachingAssistants();
        taTable.setItems(tableData);
        String undergradColumnText = props.getProperty(TAManagerProp.UNDERGRAD_TEXT.toString());
        String nameColumnText = props.getProperty(TAManagerProp.NAME_COLUMN_TEXT.toString());
        String emailColumnText = props.getProperty(TAManagerProp.EMAIL_COLUMN_TEXT.toString());
        nameColumn = new TableColumn(nameColumnText);
        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(.3));
        emailColumn = new TableColumn(emailColumnText);
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(.3));
        TableColumn undergradColumn = new TableColumn(undergradColumnText);
        undergradColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(.3));
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("name")
        );
        emailColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("email")
        );
        undergradColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TeachingAssistant, CheckBox>, ObservableValue<CheckBox>>() {

            @Override
            public ObservableValue<CheckBox> call(
                TableColumn.CellDataFeatures<TeachingAssistant, CheckBox> arg0) {
                TeachingAssistant ta = arg0.getValue();

                CheckBox checkBox = new CheckBox();

                checkBox.selectedProperty().setValue(ta.getUndergrad());



                checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                    public void changed(ObservableValue<? extends Boolean> ov,
                            Boolean old_val, Boolean new_val) {

                        ta.setUndergrad(new_val);
                        System.out.println(ta.getName()+ta.getUndergrad());

                    }
                });

                return new SimpleObjectProperty<CheckBox>(checkBox);

            }

        });
        
        taTable.getColumns().add(nameColumn);
        taTable.getColumns().add(emailColumn);
        taTable.getColumns().add(undergradColumn);

        // ADD BOX FOR ADDING A TA
        String namePromptText = props.getProperty(TAManagerProp.NAME_PROMPT_TEXT.toString());
        String emailPromptText = props.getProperty(TAManagerProp.EMAIL_PROMPT_TEXT.toString());
        String startHourPromptText = props.getProperty(TAManagerProp.START_HOUR_PROMPT_TEXT.toString());
        String endHourPromptText = props.getProperty(TAManagerProp.END_HOUR_PROMPT_TEXT.toString());
        String addButtonText = props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString());
        String updateTaButtonText = props.getProperty(TAManagerProp.UPDATE_TA_BUTTON_TEXT.toString());
        String clearButtonText = props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString());
        String changeTimeButtonText = props.getProperty(TAManagerProp.CHANGE_TIME_BUTTON_TEXT.toString());

        changeTimeButton = new Button(changeTimeButtonText);
        newStartTime = new ComboBox();
        newEndTime = new ComboBox();
        for (int i = 0; i < 24; i++) {
            newStartTime.getItems().addAll(buildCellText(i, "00"));
        }
        for (int i = 0; i < 24; i++) {
            newEndTime.getItems().addAll(buildCellText(i, "00"));
        }

        nameTextField = new TextField();
        emailTextField = new TextField();
        nameTextField.setPromptText(namePromptText);
        emailTextField.setPromptText(emailPromptText);
        addButton = new Button(addButtonText);
        updateTaButton = new Button(updateTaButtonText);
        clearButton = new Button(clearButtonText);
        clearButton1 = new Button(clearButtonText);
        addBox = new HBox();
        newStartTime.setPromptText(startHourPromptText);
        newEndTime.setPromptText(endHourPromptText);

        nameTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        emailTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        addButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        updateTaButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        clearButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        updateTaButton.prefHeightProperty().bind(addBox.heightProperty().multiply(1));
        clearButton.prefHeightProperty().bind(addBox.heightProperty().multiply(1));
        clearButton1.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        clearButton1.prefHeightProperty().bind(addBox.heightProperty().multiply(1));

        addBox.getChildren().add(nameTextField);
        addBox.getChildren().add(emailTextField);
        addBox.getChildren().add(addButton);
        addBox.getChildren().add(clearButton1);

        // INIT THE HEADER ON THE RIGHT
        officeHoursHeaderBox = new HBox();
        String officeHoursGridText = props.getProperty(TAManagerProp.OFFICE_HOURS_SUBHEADER.toString());
        officeHoursHeaderLabel = new Label(officeHoursGridText);
        officeHoursHeaderBox.getChildren().add(officeHoursHeaderLabel);
        officeHoursHeaderBox.getChildren().add(newStartTime);
        officeHoursHeaderBox.getChildren().add(newEndTime);
        officeHoursHeaderBox.getChildren().add(changeTimeButton);

        // THESE WILL STORE PANES AND LABELS FOR OUR OFFICE HOURS GRID
        officeHoursGridPane = new GridPane();
        officeHoursGridTimeHeaderPanes = new HashMap();
        officeHoursGridTimeHeaderLabels = new HashMap();
        officeHoursGridDayHeaderPanes = new HashMap();
        officeHoursGridDayHeaderLabels = new HashMap();
        officeHoursGridTimeCellPanes = new HashMap();
        officeHoursGridTimeCellLabels = new HashMap();
        officeHoursGridTACellPanes = new HashMap();
        officeHoursGridTACellLabels = new HashMap();

        // ORGANIZE THE LEFT AND RIGHT PANES
        VBox leftPane = new VBox();
        leftPane.getChildren().add(tasHeaderBox);
        leftPane.getChildren().add(taTable);
        leftPane.getChildren().add(addBox);
        VBox rightPane = new VBox();
        rightPane.getChildren().add(officeHoursHeaderBox);
        rightPane.getChildren().add(officeHoursGridPane);

        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane sPane = new SplitPane(leftPane, new ScrollPane(rightPane));
        workspace = new BorderPane();

        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(sPane);
        initCourseDetailsComponent();
        initRecitationComponent();
        initScheduleComponent();
        initProjectsComponent();
        // MAKE SURE THE TABLE EXTENDS DOWN FAR ENOUGH
        taTable.prefHeightProperty().bind(workspace.heightProperty().multiply(1.9));
        
        //Sets the tab to the current workspace pane
        super.taDataTab.setContent(workspace);

        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new TAController(app);

        // CONTROLS FOR ADDING TAs
        nameTextField.setOnAction(e -> {
            controller.handleAddTA();
        });
        emailTextField.setOnAction(e -> {
            controller.handleAddTA();
        });
        addButton.setOnAction(e -> {
            controller.handleAddTA();
        });
        removeTA.setOnAction(e -> {
            controller.deleteTA();
            nameTextField.setText("");
            emailTextField.setText("");
            
        });
        changeTimeButton.setOnAction(e -> {
            String startTime = (String) newStartTime.getValue();
            String endTime = (String) newEndTime.getValue();
            System.out.println(startTime);
            System.out.println(endTime);
            controller.handleChangeTime(startTime, endTime);

        });
        updateTaButton.setOnAction(e -> {
            controller.handleUpdateTA();
        });
        clearButton.setOnAction(e -> {
            addBox.getChildren().add(addButton);
            addBox.getChildren().add(clearButton1);
            addBox.getChildren().remove(updateTaButton);
            addBox.getChildren().remove(clearButton);
            nameTextField.clear();
            emailTextField.clear();
            nameTextField.setPromptText(namePromptText);
            emailTextField.setPromptText(emailPromptText);

        });
        clearButton1.setOnAction(e -> {
            nameTextField.clear();
            emailTextField.clear();
            nameTextField.setPromptText(namePromptText);
            emailTextField.setPromptText(emailPromptText);
        });

        workspace.setOnKeyPressed(e -> {
            if (e.isControlDown() && e.getCode() == (KeyCode.Y)) {
                System.out.println("Workspace Control Y");
                controller.handleReDoTransaction();
            } else if (e.isControlDown() && e.getCode() == (KeyCode.Z)) {
                System.out.println("Workspace Control Z Pressed");
                controller.handleUndoTransaction();
            }

        });

        taTable.setFocusTraversable(true);
        taTable.setOnKeyPressed(e -> {
            controller.handleKeyPress(e.getCode());
        });
        taTable.setOnMousePressed(e -> {
            addBox.getChildren().clear();
            controller.handleTaClicked(workspace, addBox);
            System.out.println("Clicked TA");
        });
            //Fixes issues with comboboxes and touchscreen
        newStartTime.setOnMousePressed(e -> {
                newStartTime.requestFocus();
        });
        newEndTime.setOnMousePressed(e -> {
                newEndTime.requestFocus();
        });

    }

    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    public HBox getTAsHeaderBox() {
        return tasHeaderBox;
    }

    public Label getTAsHeaderLabel() {
        return tasHeaderLabel;
    }

    public TableView getTATable() {
        return taTable;
    }

    public HBox getAddBox() {
        return addBox;
    }

    public TextField getNameTextField() {
        return nameTextField;
    }

    public TextField getEmailTextField() {
        return emailTextField;
    }

    public Button getAddButton() {
        return addButton;
    }

    public Button getUpdateTaButton() {
        return updateTaButton;
    }

    public Button getClearButton() {
        return clearButton;
    }

    public Button getClearButton1() {
        return clearButton1;
    }

    public Button getChangeTimeButton() {
        return changeTimeButton;
    }

    public ComboBox getNewStartBox() {
        return newStartTime;
    }

    public ComboBox getNewEndBox() {
        return newEndTime;

    }

    public HBox getOfficeHoursSubheaderBox() {
        return officeHoursHeaderBox;
    }

    public TAController getController() {
        return controller;
    }

    public Label getOfficeHoursSubheaderLabel() {
        return officeHoursHeaderLabel;
    }

    public GridPane getOfficeHoursGridPane() {
        return officeHoursGridPane;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeHeaderPanes() {
        return officeHoursGridTimeHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeHeaderLabels() {
        return officeHoursGridTimeHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridDayHeaderPanes() {
        return officeHoursGridDayHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridDayHeaderLabels() {
        return officeHoursGridDayHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeCellPanes() {
        return officeHoursGridTimeCellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeCellLabels() {
        return officeHoursGridTimeCellLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTACellPanes() {
        return officeHoursGridTACellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTACellLabels() {
        return officeHoursGridTACellLabels;
    }

    public String getCellKey(Pane testPane) {
        for (String key : officeHoursGridTACellLabels.keySet()) {
            if (officeHoursGridTACellPanes.get(key) == testPane) {
                return key;
            }
        }
        return null;
    }

    public Label getTACellLabel(String cellKey) {
        return officeHoursGridTACellLabels.get(cellKey);
    }

    public Pane getTACellPane(String cellPane) {
        return officeHoursGridTACellPanes.get(cellPane);
    }

    public String buildCellKey(int col, int row) {
        return "" + col + "_" + row;
    }

    public String buildCellText(int militaryHour, String minutes) {
        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutes;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }

    @Override
    public void resetWorkspace() {
        // CLEAR OUT THE GRID PANE
        officeHoursGridPane.getChildren().clear();

        // AND THEN ALL THE GRID PANES AND LABELS
        officeHoursGridTimeHeaderPanes.clear();
        officeHoursGridTimeHeaderLabels.clear();
        officeHoursGridDayHeaderPanes.clear();
        officeHoursGridDayHeaderLabels.clear();
        officeHoursGridTimeCellPanes.clear();
        officeHoursGridTimeCellLabels.clear();
        officeHoursGridTACellPanes.clear();
        officeHoursGridTACellLabels.clear();
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        TAData taData = (TAData) dataComponent;
        reloadOfficeHoursGrid(taData);
        recitationList.refresh();
        studentTable.refresh();
        projectTable.refresh();
        scheduleTable.refresh();
        
    }
    public void refreshTables(AppDataComponent dataComponent){
        TAData taData = (TAData) dataComponent;
        ObservableList<Recitation> recitations = FXCollections.observableArrayList(taData.getRecitations());
        recitationList.setItems(recitations);
    }

    public void reloadOfficeHoursGrid(TAData dataComponent) {
        ArrayList<String> gridHeaders = dataComponent.getGridHeaders();

        // ADD THE TIME HEADERS
        for (int i = 0; i < 2; i++) {
            addCellToGrid(dataComponent, officeHoursGridTimeHeaderPanes, officeHoursGridTimeHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }

        // THEN THE DAY OF WEEK HEADERS
        for (int i = 2; i < 7; i++) {
            addCellToGrid(dataComponent, officeHoursGridDayHeaderPanes, officeHoursGridDayHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }

        // THEN THE TIME AND TA CELLS
        int row = 1;
        for (int i = dataComponent.getStartHour(); i < dataComponent.getEndHour(); i++) {
            // START TIME COLUMN
            int col = 0;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(i, "00"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row + 1);
            dataComponent.getCellTextProperty(col, row + 1).set(buildCellText(i, "30"));

            // END TIME COLUMN
            col++;
            int endHour = i;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(endHour, "30"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row + 1);
            dataComponent.getCellTextProperty(col, row + 1).set(buildCellText(endHour + 1, "00"));
            col++;

            // AND NOW ALL THE TA TOGGLE CELLS
            while (col < 7) {
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row);
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row + 1);
                col++;
            }
            row += 2;
        }

        // CONTROLS FOR TOGGLING TA OFFICE HOURS
        for (Pane p : officeHoursGridTACellPanes.values()) {
            p.setFocusTraversable(true);
            p.setOnKeyPressed(e -> {
                controller.handleKeyPress(e.getCode());
            });
            p.setOnMouseClicked(e -> {
                controller.handleCellToggle((Pane) e.getSource());
            });
            p.setOnMouseExited(e -> {
                controller.handleGridCellMouseExited((Pane) e.getSource());
            });
            p.setOnMouseEntered(e -> {
                controller.handleGridCellMouseEntered((Pane) e.getSource());
            });
        }

        // AND MAKE SURE ALL THE COMPONENTS HAVE THE PROPER STYLE
        TAStyle taStyle = (TAStyle) app.getStyleComponent();
        taStyle.initOfficeHoursGridStyle();
    }

    public void addCellToGrid(TAData dataComponent, HashMap<String, Pane> panes, HashMap<String, Label> labels, int col, int row) {
        // MAKE THE LABEL IN A PANE
        Label cellLabel = new Label("");
        HBox cellPane = new HBox();
        cellPane.setAlignment(Pos.CENTER);
        cellPane.getChildren().add(cellLabel);

        // BUILD A KEY TO EASILY UNIQUELY IDENTIFY THE CELL
        String cellKey = dataComponent.getCellKey(col, row);
        cellPane.setId(cellKey);
        cellLabel.setId(cellKey);

        // NOW PUT THE CELL IN THE WORKSPACE GRID
        officeHoursGridPane.add(cellPane, col, row);

        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
        panes.put(cellKey, cellPane);
        labels.put(cellKey, cellLabel);

        // AND FINALLY, GIVE THE TEXT PROPERTY TO THE DATA MANAGER
        // SO IT CAN MANAGE ALL CHANGES
        dataComponent.setCellProperty(col, row, cellLabel.textProperty());
    }
    
    public void initCourseDetailsComponent(){
        //Has all of the gui for the course details page
        //Strings from XML file
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //Section one
        String courseInfoString = props.getProperty(TAManagerProp.COURSE_INFO_TEXT.toString());
        String subjectString = props.getProperty(TAManagerProp.SUBJECT_INFO_TEXT.toString());
        String number = props.getProperty(TAManagerProp.NUMBER_INFO_TEXT.toString());
        String semester = props.getProperty(TAManagerProp.SEMESTER_INFO_TEXT.toString());
        String year = props.getProperty(TAManagerProp.YEAR_INFO_TEXT.toString());
        String title = props.getProperty(TAManagerProp.TITLE_INFO_TEXT.toString());
        String instructorName = props.getProperty(TAManagerProp.INSTRUCTOR_NAME_INFO_TEXT.toString());
        String instructorHome = props.getProperty(TAManagerProp.INSTRUCTOR_HOME_INFO_TEXT.toString());
        String exportDir = props.getProperty(TAManagerProp.EXPORT_DIR_INFO_TEXT.toString());
        //Section two
        String siteTemplate = props.getProperty(TAManagerProp.SITE_TEMPLATE_INFO_TEXT.toString());
        String siteTemplateInstruction = props.getProperty(TAManagerProp.SITE_TEMPLATE_INSTRUCTION_TEXT.toString());
        String selectDirectory = props.getProperty(TAManagerProp.SELECT_DIRECTORY.toString());
        String sitePages = props.getProperty(TAManagerProp.SITE_PAGES_INFO_TEXT.toString());
        String use = props.getProperty(TAManagerProp.USE_INFO_TEXT.toString());
        String navTitle = props.getProperty(TAManagerProp.NAVBAR_INFO_TEXT.toString());
        String fileName = props.getProperty(TAManagerProp.FILENAME_INFO_TEXT.toString());
        String script = props.getProperty(TAManagerProp.SCRIPT_INFO_TEXT.toString());
        //Section three
        String pageStyle = props.getProperty(TAManagerProp.PAGE_STYLE_INFO_TEXT.toString());
        String bannerImage = props.getProperty(TAManagerProp.BANNER_INFO_TEXT.toString());
        String footerLeft = props.getProperty(TAManagerProp.FOOTER_LEFT_INFO_TEXT.toString());
        String footerRight = props.getProperty(TAManagerProp.FOOTER_RIGHT_INFO_TEXT.toString());
        String change = props.getProperty(TAManagerProp.CHANGE_BUTTON.toString());
        String styleSheet = props.getProperty(TAManagerProp.STYLESHEET_INFO_TEXT.toString());
        String styleSheetInstruction = props.getProperty(TAManagerProp.STYLESHEET_INSTRUCTION_TEXT.toString());
        
        //Box to hold all the sections
        BorderPane border = new BorderPane();
        //border.setPadding(new Insets(15,75,15,75));
        VBox courseInfoPane = new VBox();
        border.setCenter(courseInfoPane);
        
        //The first section
        VBox courseInfo = new VBox();
        
        courseInfo.setPadding(new Insets(0,0,50,0));
        //Box for title
        VBox courseInfoTitle = new VBox();
        courseInfoTitle.getStyleClass().add("hbox");
        courseInfoTitle.setPadding(new Insets(0,0,10,0));
        Label courseInfoLabel = new Label(courseInfoString);
        
        courseInfoLabel.getStyleClass().add("title_text");
        courseInfoLabel.setAlignment(Pos.CENTER);
        courseInfoTitle.getChildren().add(courseInfoLabel);       

        
        //Box for the first row of controls
        HBox firstLine = new HBox();
        ScrollPane scrollPane = new ScrollPane();
        firstLine.prefWidthProperty().bind(scrollPane.widthProperty().multiply(1));
        //For the subject
        HBox subjectControl = new HBox();
        Label subjectLabel = new Label(subjectString);
        //Subjects for combobox
        ObservableList subjects = FXCollections.observableArrayList();
        subjects.addAll("AAS","AMS","ARS","CSE","CCS","EGL","ISE","MAT");
        subjectPicker = new ComboBox(subjects);
        subjectPicker.setOnMousePressed(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                subjectPicker.requestFocus();
            }
        });
        subjectControl.getChildren().add(subjectLabel);
        subjectControl.getChildren().add(subjectPicker);
        subjectControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        subjectLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //subjectLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        subjectPicker.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        
        //For the number
        HBox numberControl = new HBox();
        Label numberLabel = new Label(number);
        ObservableList numbers = FXCollections.observableArrayList();
        for(int i=100;i<=350;i++){
            String s = Integer.toString(i);
            numbers.add(s);
        
        }
        numberPicker = new ComboBox(numbers);
        numberPicker.setOnMousePressed(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                numberPicker.requestFocus();
            }
        });
        numberControl.getChildren().add(numberLabel);
        numberControl.getChildren().add(numberPicker);
        numberControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        numberLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //numberLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        numberPicker.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //Add them 
        firstLine.getChildren().add(subjectControl);
        firstLine.getChildren().add(numberControl);
        firstLine.setPadding(new Insets(0,0,5,0));
        firstLine.getStyleClass().add("hbox");
        //For the second row
        HBox secondLine = new HBox();
        secondLine.setPadding(new Insets(0,0,5,0));
        //For the semester
        HBox semesterControl = new HBox();
        Label semesterLabel = new Label(semester);
        ObservableList semesters = FXCollections.observableArrayList();
        semesters.addAll("Fall","Spring","Winter","Summer");
        semesterPicker = new ComboBox(semesters);
        semesterPicker.setOnMousePressed(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                semesterPicker.requestFocus();
            }
        });
        semesterControl.getChildren().add(semesterLabel);
        semesterControl.getChildren().add(semesterPicker);
        semesterControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        semesterLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //semesterLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        semesterPicker.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //For the Year
        HBox yearControl = new HBox();
        Label yearLabel = new Label(year);
        ObservableList years = FXCollections.observableArrayList();
        for(int i=2017;i<=2027;i++){
            String s = Integer.toString(i);
            years.add(s);
        
        }
        yearPicker = new ComboBox(years);
        yearPicker.setOnMousePressed(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                yearPicker.requestFocus();
            }
        });
        yearControl.getChildren().add(yearLabel);
        yearControl.getChildren().add(yearPicker);
        yearControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        yearLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //yearLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        yearPicker.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        
        secondLine.getChildren().add(semesterControl);
        secondLine.getChildren().add(yearControl);
        secondLine.getStyleClass().add("hbox");
        //For the third row
        HBox thirdLine = new HBox();
        thirdLine.getStyleClass().add("hbox");
        thirdLine.setPadding(new Insets(0,0,5,0));
        Label titleLabel = new Label(title);
        courseTitleInput = new TextField();
        thirdLine.getChildren().add(titleLabel);
        thirdLine.getChildren().add(courseTitleInput);
        courseTitleInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.75));
        titleLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.25));
        //titleLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        //For the fourth row
        HBox fourthLine = new HBox();
        fourthLine.getStyleClass().add("hbox");
        fourthLine.setPadding(new Insets(0,0,5,0));
        Label instructorNameLabel = new Label(instructorName);
        nameInput = new TextField();
        fourthLine.getChildren().add(instructorNameLabel);
        fourthLine.getChildren().add(nameInput);
        nameInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.75));
        instructorNameLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.25));
        //instructorNameLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        //Fifth row
        HBox fifthLine = new HBox();
        fifthLine.getStyleClass().add("hbox");
        fifthLine.setPadding(new Insets(0,0,5,0));
        Label instructorHomeLabel = new Label(instructorHome);
        homeInput = new TextField();
        fifthLine.getChildren().add(instructorHomeLabel);
        fifthLine.getChildren().add(homeInput);
        homeInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.75));
        instructorHomeLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.25));
        //instructorHomeLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        //Sixth
        HBox sixthLine = new HBox();
        sixthLine.getStyleClass().add("hbox");
        sixthLine.setPadding(new Insets(0,0,5,0));
        Label exportDirLabel = new Label(exportDir);
        exportDirPlaceholder = new Label("DIRECTORY TEXT PLACEHOLDER");
        Button changeExportDir = new Button(change);
        sixthLine.getChildren().add(exportDirLabel);
        sixthLine.getChildren().add(exportDirPlaceholder);
        sixthLine.getChildren().add(changeExportDir);
        exportDirPlaceholder.prefWidthProperty().bind(firstLine.widthProperty().multiply(.65));
        //exportDirPlaceholder.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        exportDirLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.25));
        //exportDirLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        //Add to first section
        HBox seventhLine = new HBox();
        Button updateCourseInfo = new Button("Update Info");
        seventhLine.getChildren().add(updateCourseInfo);
        courseInfo.getChildren().add(courseInfoTitle);
        courseInfo.getChildren().add(firstLine);
        courseInfo.getChildren().add(secondLine);
        courseInfo.getChildren().add(thirdLine);
        courseInfo.getChildren().add(fourthLine);
        courseInfo.getChildren().add(fifthLine);
        courseInfo.getChildren().add(sixthLine);
        courseInfo.getChildren().add(seventhLine);
        
        courseInfoPane.getChildren().add(courseInfo);
        
        //Second Section
        //The first section
        VBox siteTemplateBox = new VBox(); 
        siteTemplateBox.setPadding(new Insets(0,0,50,0));
        //Box for title
        VBox siteTemplateTitle = new VBox();
        
        siteTemplateTitle.getStyleClass().add("hbox");
        siteTemplateTitle.setPadding(new Insets(0,0,5,0));
        Label siteTemplateLabel = new Label(siteTemplate);
        siteTemplateLabel.getStyleClass().add("title_text");
        siteTemplateTitle.getChildren().add(siteTemplateLabel);
        siteTemplateTitle.setPadding(new Insets(0,0,5,0));
        
        //Box for the select template 
        VBox selectTemplateControl = new VBox();
        selectTemplateControl.getStyleClass().add("hbox");
        selectTemplateControl.setPadding(new Insets(0,0,5,0));
        Label templateSelectInstructions = new Label(siteTemplateInstruction);
        templateDirPlaceholder = new Label("TEMPLATE DIRECTORY PLACEHOLDER");
        Button chooseTemplateDirectory = new Button(selectDirectory);
        selectTemplateControl.getChildren().add(templateSelectInstructions);
        selectTemplateControl.getChildren().add(templateDirPlaceholder);
        selectTemplateControl.getChildren().add(chooseTemplateDirectory);
        templateDirPlaceholder.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        //templateDirPlaceholder.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        chooseTemplateDirectory.prefWidthProperty().bind(firstLine.widthProperty().multiply(.2));
        
        //Second section
        VBox sitePagesControl = new VBox();
        sitePagesControl.getStyleClass().add("hbox");
        sitePagesControl.setPadding(new Insets(5,0,5,0));
        Label sitePagesLabel = new Label(sitePages);
        //Box for the site pages
        sitePagesList = new TableView();
        //HBox tablePane = new HBox();

        
 
        TableColumn useColumn = new TableColumn(use);
        
        useColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Page, CheckBox>, ObservableValue<CheckBox>>() {

            @Override
            public ObservableValue<CheckBox> call(
                TableColumn.CellDataFeatures<Page, CheckBox> arg0) {
                Page p = arg0.getValue();

                CheckBox checkBox = new CheckBox();

                checkBox.selectedProperty().setValue(p.getUse());



                checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                    public void changed(ObservableValue<? extends Boolean> ov,
                            Boolean old_val, Boolean new_val) {

                        p.setUse(new_val);

                    }
                });

                return new SimpleObjectProperty<CheckBox>(checkBox);

            }

        });
        
        ObservableList<Page> data = FXCollections.observableArrayList(
                new Page(true,"Home","index.html","HomeBuilder.js"),
                new Page(true,"Syllabus","Syllabus.html","SyllabusBuilder.js"),
                new Page(true,"Schedule","Schedule.html","ScheduleBuilder.js"),
                new Page(true,"HWs","hws.html","HWsBuilder.js"),
                new Page(true,"Projects","projects.html","ProjectsBuilder.js")
        );
        
        
        TableColumn navbarColumn = new TableColumn(navTitle);
        navbarColumn.setCellValueFactory(
                new PropertyValueFactory<>("title"));
        
        TableColumn fileColumn = new TableColumn(fileName);
        fileColumn.setCellValueFactory(
                new PropertyValueFactory<>("file"));
        
        TableColumn scriptColumn = new TableColumn(script);
        scriptColumn.setCellValueFactory(
                new PropertyValueFactory<>("script"));
        
        sitePagesList.setItems(data);
        
        sitePagesList.getColumns().addAll(useColumn,navbarColumn,fileColumn,scriptColumn);
        
        sitePagesControl.getChildren().add(sitePagesLabel);
        sitePagesControl.getStyleClass().add("hbox");
        sitePagesControl.getChildren().add(sitePagesList);
        sitePagesControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        siteTemplateBox.getChildren().add(siteTemplateTitle);
        siteTemplateBox.getChildren().add(selectTemplateControl);
        siteTemplateBox.getChildren().add(sitePagesControl);
        
        courseInfoPane.getChildren().add(siteTemplateBox);
        
        //Third section
        VBox pageStyleBox = new VBox();
        pageStyleBox.setPadding(new Insets(0,0,50,0));
        VBox pageStyleTitle = new VBox();
        pageStyleTitle.getStyleClass().add("hbox");
        Label pageStyleTitleLabel = new Label(pageStyle);
        pageStyleTitleLabel.getStyleClass().add("title_text");
        pageStyleTitle.getChildren().add(pageStyleTitleLabel);
        pageStyleBox.getChildren().add(pageStyleTitle);
        pageStyleTitle.setPadding(new Insets(0,0,5,0));
        //First row
        HBox bannerImageControl = new HBox();
        bannerImageControl.getStyleClass().add("hbox");
        Label bannerImageLabel = new Label(bannerImage);
        bannerImageDisplay = new ImageView();
        Button changeBanner = new Button(change);
        bannerImageControl.getChildren().addAll(bannerImageLabel,bannerImageDisplay,changeBanner);
        bannerImageLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //changeBanner.prefHeightProperty().bind(firstLine.heightProperty().multiply(.2));
        bannerImageControl.setPadding(new Insets(0,0,5,0));
        
        //Second Row
        HBox leftFooterImageControl = new HBox();
        leftFooterImageControl.getStyleClass().add("hbox");
        Label leftFooterImageLabel = new Label(footerLeft);
        leftFooterImageDisplay = new ImageView();
        Button changeLeftFooter = new Button(change);
        leftFooterImageControl.getChildren().addAll(leftFooterImageLabel,leftFooterImageDisplay,changeLeftFooter);
        leftFooterImageLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //changeLeftFooter.prefHeightProperty().bind(firstLine.heightProperty().multiply(.2));
        leftFooterImageControl.setPadding(new Insets(0,0,5,0));
        //Third Row
        HBox rightFooterImageControl = new HBox();
        rightFooterImageControl.getStyleClass().add("hbox");
        Label rightFooterImageLabel = new Label(footerRight);
        rightFooterImageDisplay = new ImageView();
        Button changeRightFooter = new Button(change);
        rightFooterImageControl.getChildren().addAll(rightFooterImageLabel,rightFooterImageDisplay,changeRightFooter);
        rightFooterImageControl.setPadding(new Insets(0,0,5,0));
        rightFooterImageLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //changeRightFooter.prefHeightProperty().bind(firstLine.heightProperty().multiply(.2));
        //Stylesheet controls
        
        //For getting the files for the combobox
        ObservableList cssFiles = FXCollections.observableArrayList();
        File directoryTest = new File("work/css");
        //get all the files from a directory
        File[] fList = directoryTest.listFiles();
        for (File file : fList){
            if (file.isFile()){
                System.out.println(file.getName());
                cssFiles.add(file.getName());
            }
        }

        
        HBox styleSheetControl = new HBox();
        styleSheetControl.getStyleClass().add("hbox");
        Label styleSheetLabel = new Label(styleSheet);
        styleSheetLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));       
        stylesheetPicker = new ComboBox(cssFiles);

        stylesheetPicker.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        Button stylesheetUpdate = new Button("Update stylesheet");
        styleSheetControl.getChildren().addAll(styleSheetLabel,stylesheetPicker,stylesheetUpdate);
        styleSheetControl.setPadding(new Insets(0,0,5,0));
        Label styleSheetInstrucionLabel = new Label(styleSheetInstruction);
        styleSheetInstrucionLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(1));
        styleSheetInstrucionLabel.getStyleClass().add("hbox");

        //add
        pageStyleBox.getChildren().addAll(bannerImageControl,leftFooterImageControl,rightFooterImageControl,styleSheetControl,styleSheetInstrucionLabel);
        courseInfoPane.getChildren().add(pageStyleBox);
        scrollPane.setContent(courseInfoPane);
        courseInfoPane.prefWidthProperty().bind(scrollPane.widthProperty().multiply(1));
        courseInfoPane.setPadding(new Insets(50,100,50,100));
        courseInfoPane.setAlignment(Pos.CENTER);
        courseInfoPane.getStyleClass().add("background_pane");
        super.courseDetailsTab.setContent(scrollPane);
        //For the controller
        stylesheetPicker.setOnMousePressed(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                stylesheetPicker.requestFocus();
            }
        });
        stylesheetUpdate.setOnAction(e ->{ 
                controller.handleCSSSheetChange();
        });
        changeExportDir.setOnAction(e -> {
                DirectoryChooser directoryChooser = new DirectoryChooser();
                AppGUI gui = app.getGUI();
                File selectedDirectory = directoryChooser.showDialog(gui.getPrimaryStage());
                String directory = selectedDirectory.getAbsolutePath();
                controller.handleExportDirectorySelected(directory,exportDirPlaceholder);
            }
        );
        chooseTemplateDirectory.setOnAction(e -> {
                DirectoryChooser directoryChooser = new DirectoryChooser();
                AppGUI gui = app.getGUI();
                File selectedDirectory = directoryChooser.showDialog(gui.getPrimaryStage());
                String directory = selectedDirectory.getAbsolutePath();
                controller.handleTemplateDirectorySelected(directory,templateDirPlaceholder);
            }
        );
        changeBanner.setOnAction(e -> {
                AppGUI gui = app.getGUI();
                FileChooser fileChooser = new FileChooser();      
                fileChooser.setTitle("Select Banner Image");
                  
                fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png"));
                File selectedImage = fileChooser.showOpenDialog(gui.getPrimaryStage());
                String directory = selectedImage.getPath();
            try {
                controller.handleBannerSelected(directory,selectedImage);
            } 
            catch (MalformedURLException ex) {
                Logger.getLogger(TAWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
            }
        );
        changeLeftFooter.setOnAction(e -> {
                AppGUI gui = app.getGUI();
                FileChooser fileChooser = new FileChooser();      
                fileChooser.setTitle("Select Left Footer Image");
                  
                fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png"));
                File selectedImage = fileChooser.showOpenDialog(gui.getPrimaryStage());
                String directory = selectedImage.getPath();
            try {
                controller.handleLeftFooterSelected(directory,selectedImage);
            } 
            catch (MalformedURLException ex) {
                Logger.getLogger(TAWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
            }
        );
        changeRightFooter.setOnAction(e -> {
                AppGUI gui = app.getGUI();
                FileChooser fileChooser = new FileChooser();      
                fileChooser.setTitle("Select Right Footer Image");
                  
                fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png"));
                File selectedImage = fileChooser.showOpenDialog(gui.getPrimaryStage());
                String directory = selectedImage.getPath();
            try {
                controller.handleRightFooterSelected(directory,selectedImage);
            } 
            catch (MalformedURLException ex) {
                Logger.getLogger(TAWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
            }
        );
        updateCourseInfo.setOnAction(e -> {
                controller.handleUpdateCourseInfo();
            }
        );
    }
    //For the page table
    public static class Page{
        private SimpleBooleanProperty use;
        private final SimpleStringProperty title;
        private final SimpleStringProperty file;
        private final SimpleStringProperty script;
        Page(boolean use, String title, String file, String script){
            this.use = new SimpleBooleanProperty(use);
            this.file = new SimpleStringProperty(file);
            this.title = new SimpleStringProperty(title);
            this.script = new SimpleStringProperty(script);
        }
        public String getTitle(){
            return title.get();
        }
        public String getFile(){
            return file.get();
        }
        public String getScript(){
            return script.get();
        }
        public Boolean getUse(){
            return use.get();
        }
        public void setUse(boolean initUse){
            use.set(initUse);
        }
    }
    
    public ComboBox getSubjectBox(){
        return subjectPicker;
    }
    public ComboBox getNumberBox(){
        return numberPicker;
    }
    public ComboBox getSemesterBox(){
        return semesterPicker;
    }
    public ComboBox getYearBox(){
        return yearPicker;
    }
    public TextField getCourseTitleBox(){
        return courseTitleInput;
    }
    public TextField getInstructorNameBox(){
        return nameInput;
    }
    public TextField getInstructorHomeBox(){
        return homeInput;
    }
    public Label getExportDirectoryBox(){
        return exportDirPlaceholder;
    }
    
    public Label getTemplateDirectoryBox(){
        return templateDirPlaceholder;
    }
    public TableView getSitePagesList(){
        return sitePagesList;
    }
    public Label getBannerImagePlaceholder(){
        return bannerImagePlaceholder;
    }
    public Label getLeftFooterImagePlaceholder(){
        return leftFooterImagePlaceholder;
    }
    public Label getRightFooterImagePlaceholder(){
        return rightFooterImagePlaceholder;
    }
    public ComboBox getStylesheetPicker(){
        return stylesheetPicker;
    }
    
    
     public void initProjectsComponent(){
        //Has all of the gui for the course details page
        //Strings from XML file
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //Section one
        String projectsTitle = props.getProperty(TAManagerProp.PROJECTS_TITLE.toString());
        String teams = props.getProperty(TAManagerProp.TEAMS_TITLE.toString());
        String name = props.getProperty(TAManagerProp.NAME_COLUMN_TEXT.toString());
        String color = props.getProperty(TAManagerProp.COLOR_TITLE.toString());
        String textColor = props.getProperty(TAManagerProp.TEXT_COLOR_TITLE.toString());
        String link = props.getProperty(TAManagerProp.LINK_TITLE.toString());
        String addTitle = props.getProperty(TAManagerProp.EDIT_TITLE.toString());
        //Reuse color and text color and link
        String addButton = props.getProperty(TAManagerProp.ADD_UPDATE_BUTTON_TEXT.toString());
        String clearButton = props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString());
        //Second section
        String students = props.getProperty(TAManagerProp.STUDENTS_TITLE.toString());
        String firstName = props.getProperty(TAManagerProp.FIRST_NAME_TITLE.toString());
        String lastName = props.getProperty(TAManagerProp.LAST_NAME_TITLE.toString());
        //Reuse teams title
        String role = props.getProperty(TAManagerProp.ROLE_TITLE.toString());
        String editTitle = props.getProperty(TAManagerProp.EDIT_TITLE.toString());
        //Reuse the first name, last name, team, and role as well as button labels
        
        //Box to hold all the sections
        BorderPane border = new BorderPane();
        //border.setPadding(new Insets(15,75,15,75));
        VBox projectsPane = new VBox();
        border.setCenter(projectsPane);
        
        //The first section
        VBox projectsInfo = new VBox();
        projectsInfo.setPadding(new Insets(0,0,50,0));
        projectsPane.getChildren().add(projectsInfo);
        //Box for title
        VBox projectsInfoTitle = new VBox();
        projectsInfoTitle.getStyleClass().add("hbox");
        projectsInfoTitle.setPadding(new Insets(0,0,10,0));
        Label projectsInfoLabel = new Label(projectsTitle);
        Button removeProject = new Button("-");
        projectsInfoTitle.getChildren().add(removeProject);
        
        projectsInfoLabel.getStyleClass().add("title_text");
        projectsInfoLabel.setAlignment(Pos.CENTER);
        projectsInfoTitle.getChildren().add(projectsInfoLabel);   
        projectsInfo.getChildren().add(projectsInfoTitle);
        //Box for the table
        VBox tableBox = new VBox();
        tableBox.getStyleClass().add("hbox");
        tableBox.setPadding(new Insets(5,0,5,0));
        //Tableview for the teams
        projectTable = new TableView();
        TableColumn nameColumn = new TableColumn(name);
        TableColumn colorColumn = new TableColumn(color);
        TableColumn colorTextColumn = new TableColumn(textColor);
        TableColumn linkColumn = new TableColumn(link);
        //Put the data in the table
        TAData data = (TAData)app.getDataComponent();
        ObservableList<Project> projectData = data.getProjects();
        projectTable.setItems(projectData);
        nameColumn.setCellValueFactory(
            new PropertyValueFactory<Project,String>("name")
        );
        colorColumn.setCellValueFactory(
            new PropertyValueFactory<Project,String>("color")
        );
        colorTextColumn.setCellValueFactory(
            new PropertyValueFactory<Project,String>("textColor")
        );
        linkColumn.setCellValueFactory(
            new PropertyValueFactory<Project,String>("link")
        );
        projectTable.getColumns().addAll(nameColumn,colorColumn,colorTextColumn,linkColumn);
        tableBox.getChildren().add(projectTable);
        projectsInfo.getChildren().add(tableBox);
        //Boxes for the controls below
        HBox firstLine = new HBox();
        ScrollPane scrollPane = new ScrollPane();
        firstLine.prefWidthProperty().bind(scrollPane.widthProperty().multiply(1));
        //For name and the link
        HBox nameControl = new HBox();
        Label nameLabel = new Label(name);
        projectNameInput = new TextField();
        nameControl.getChildren().add(nameLabel);
        nameControl.getChildren().add(projectNameInput);
        nameControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        nameLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //nameLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        projectNameInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        
        HBox linkControl = new HBox();
        Label linkLabel = new Label(link);
        projectLink = new TextField();
        linkControl.getChildren().add(linkLabel);
        linkControl.getChildren().add(projectLink);
        linkControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        linkLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //linkLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        projectLink.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //Add them 
        firstLine.getChildren().add(nameControl);
        firstLine.getChildren().add(linkControl);
        //linkControl.setAlignment(Pos.CENTER_RIGHT);
        firstLine.setPadding(new Insets(0,0,5,0));
        firstLine.getStyleClass().add("hbox");
        
        
        //For the second row
        
        HBox secondLine = new HBox();
        secondLine.setPadding(new Insets(0,0,5,0));
        //For the semester
        HBox colorControl = new HBox();
        Label colorLabel = new Label(color);
        projectColorInput = new ColorPicker();
        colorControl.getChildren().add(colorLabel);
        colorControl.getChildren().add(projectColorInput);
        colorControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        colorLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //colorLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        //colorPicker.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //For
        HBox textColorControl = new HBox();
        Label textColorLabel = new Label(textColor);
        projectTextColorInput = new ColorPicker();
        textColorControl.getChildren().add(textColorLabel);
        textColorControl.getChildren().add(projectTextColorInput);
        textColorControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        textColorLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //textColorLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        //textColorPicker.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //textColorControl.setAlignment(Pos.CENTER_RIGHT);
        secondLine.getChildren().add(colorControl);
        secondLine.getChildren().add(textColorControl);
        secondLine.getStyleClass().add("hbox");
        //For the third row
        
        HBox thirdLine = new HBox();
        thirdLine.getStyleClass().add("hbox");
        thirdLine.setPadding(new Insets(0,0,5,0));
        Button addUpdateProject = new Button(addButton);
        Button clearProject =new Button(clearButton);
        thirdLine.getChildren().addAll(addUpdateProject,clearProject);
        
        //Add to the section
        projectsInfo.getChildren().addAll(firstLine,secondLine,thirdLine);
        //Readd color pickers (secondLine) once fixed
        
        //Second Section (For the students)
        VBox studentsInfo = new VBox(); 
        studentsInfo.setPadding(new Insets(0,0,50,0));
        //Box for title
        VBox studentTitle = new VBox();
        studentTitle.getStyleClass().add("hbox");
        studentTitle.setPadding(new Insets(0,0,5,0));
        Label studentLabel = new Label(students);
        studentLabel.getStyleClass().add("title_text");
        studentTitle.getChildren().add(studentLabel);
        studentTitle.setPadding(new Insets(0,0,5,0));
        Button removeStudent = new Button("-");
        studentsInfo.getChildren().addAll(studentTitle,removeStudent);
        //Box for table 
        VBox studentsTablePane = new VBox();
        studentsTablePane.getStyleClass().add("hbox");
        studentsTablePane.setPadding(new Insets(0,0,5,0));
        //Tableview for the students
        studentTable = new TableView();
        TableColumn firstNameColumn = new TableColumn(firstName);
        TableColumn lastNameColumn = new TableColumn(lastName);
        TableColumn teamColumn = new TableColumn(teams);
        TableColumn roleColumn = new TableColumn(role);
        
        //Put the data in the table=
        ObservableList<Student> studentData = data.getStudents();
        studentTable.setItems(studentData);
        firstNameColumn.setCellValueFactory(
            new PropertyValueFactory<Student,String>("firstName")
        );
        lastNameColumn.setCellValueFactory(
            new PropertyValueFactory<Student,String>("lastName")
        );
        teamColumn.setCellValueFactory(
            new PropertyValueFactory<Student,String>("teams")
        );
        roleColumn.setCellValueFactory(
            new PropertyValueFactory<Student,String>("role")
        );
        
    
        studentTable.getColumns().addAll(firstNameColumn,lastNameColumn,teamColumn,roleColumn);
        studentsTablePane.getChildren().add(studentTable);
        studentsInfo.getChildren().add(studentsTablePane);
        
        //Box for the select template 
        VBox studentsControl = new VBox();
        studentsControl.getStyleClass().add("hbox");
        studentsControl.setPadding(new Insets(0,0,5,0));
        Label addEditTitle = new Label(addTitle);
        studentsControl.getChildren().add(addEditTitle);
        
        //First row
        HBox firstRow = new HBox();
        firstRow.prefWidthProperty().bind(scrollPane.widthProperty().multiply(1));
        //For name and the link
        HBox firstNameControl = new HBox();
        Label firstNameLabel = new Label(firstName);
        studentFirstNameInput = new TextField();
        firstNameControl.getChildren().add(firstNameLabel);
        firstNameControl.getChildren().add(studentFirstNameInput);
        firstNameControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        firstNameLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //firstNameLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        studentFirstNameInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        
        HBox lastNameControl = new HBox();
        Label lastNameLabel = new Label(lastName);
        studentLastNameInput = new TextField();
        lastNameControl.getChildren().add(lastNameLabel);
        lastNameControl.getChildren().add(studentLastNameInput);
        lastNameControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        lastNameLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //lastNameLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        studentLastNameInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //Add them 
        firstRow.getChildren().add(firstNameControl);
        firstRow.getChildren().add(lastNameControl);
        firstRow.setPadding(new Insets(0,0,5,0));
        firstRow.getStyleClass().add("hbox");
        //Second row
        HBox secondRow = new HBox();
        secondRow.prefWidthProperty().bind(scrollPane.widthProperty().multiply(1));
        //For name and the link
        HBox teamControl = new HBox();
        Label teamLabel = new Label(teams);
        //Creates the list for the Teams input
        teamsNamesList = FXCollections.observableArrayList();
        studentTeamInput = new ComboBox(teamsNamesList);
        studentTeamInput.setOnMousePressed(new EventHandler<MouseEvent>(){
            @Override
             public void handle(MouseEvent event) {
                 studentTeamInput.requestFocus();
                 controller.updateTeamsComboBox(studentTeamInput);
            }
        });
        
        teamControl.getChildren().add(teamLabel);
        teamControl.getChildren().add(studentTeamInput);
        teamControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        teamLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        teamLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        //studentTeamInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        
        HBox roleControl = new HBox();
        Label roleLabel = new Label(role);
        studentRoleInput = new TextField();
        roleControl.getChildren().add(roleLabel);
        roleControl.getChildren().add(studentRoleInput);
        roleControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        roleLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        roleLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        studentRoleInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        //Buttons
        Button addUpdateStudent = new Button(addButton);
        Button clearStudent = new Button(clearButton);
        HBox thirdRow = new HBox();
        thirdRow.getChildren().addAll(addUpdateStudent,clearStudent);
        
        //Add them 
        secondRow.getChildren().add(teamControl);
        secondRow.getChildren().add(roleControl);
        secondRow.setPadding(new Insets(0,0,5,0));
        secondRow.getStyleClass().add("hbox");
        studentsInfo.getChildren().addAll(firstRow,secondRow,thirdRow);
        
        projectsPane.getChildren().add(studentsInfo);
        //projectsPane.getChildren().add(pageStyleBox);
        scrollPane.setContent(projectsPane);
        projectsPane.prefWidthProperty().bind(scrollPane.widthProperty().multiply(1));
        projectsPane.setPadding(new Insets(50,100,50,100));
        projectsPane.setAlignment(Pos.CENTER);
        projectsPane.getStyleClass().add("background_pane");
        super.projectTab.setContent(scrollPane);
        
        
        //Stuff for the controller (projects)
        projectTable.setOnMousePressed(e -> {
            controller.handleProjectClicked();
            addUpdateProject.setText("Update");
            System.out.println("Clicked Project");
        });
        //adding projects
        projectNameInput.setOnAction(e -> {
            controller.handleAddProject(); });

        projectLink.setOnAction(e -> {
            controller.handleAddProject();
        });
        addUpdateProject.setOnAction(e -> {
            Object selectedItem = projectTable.getSelectionModel().getSelectedItem();
            if (addUpdateProject.getText().equals("Update")) {
                controller.handleEditProject();
                //Set the selections to null
                projectTable.getSelectionModel().select(null);
                projectNameInput.setText("");
                projectLink.setText("");
                projectColorInput.setValue(Color.web("WHITE"));
                projectTextColorInput.setValue(Color.web("WHITE"));
                addUpdateProject.setText("Add");
                
            }
            else{
                controller.handleAddProject();
                addUpdateProject.setText("Add");
            }
        });
        
        //Clearing selected project
        clearProject.setOnAction(e -> {
            //Set the selections to null
            projectTable.getSelectionModel().select(null);
            projectNameInput.setText("");
            projectLink.setText("");
            projectColorInput.setValue(Color.web("WHITE"));
            projectTextColorInput.setValue(Color.web("WHITE"));
            addUpdateProject.setText("Add");
            System.out.println("project info cleared");
            
        });
        //deleting projects
        removeProject.setOnAction(e -> {
            controller.deleteProject();
            projectNameInput.setText("");
            projectLink.setText("");
            projectColorInput.setValue(Color.web("WHITE"));
            projectTextColorInput.setValue(Color.web("WHITE"));
            System.out.println("project removed");
            
        });        
        projectTable.setOnKeyPressed(e -> {
            controller.handleKeyPressProject(e.getCode());
        });
        
        //Undoing stuff
        projectsPane.setOnKeyPressed(e -> {
            if (e.isControlDown() && e.getCode() == (KeyCode.Y)) {
                System.out.println("Workspace Control Y");
                controller.handleReDoTransaction();
            } else if (e.isControlDown() && e.getCode() == (KeyCode.Z)) {
                System.out.println("Workspace Control Z Pressed");
                controller.handleUndoTransaction();
            }
        });
        

        //Stuff for the controller (students)
        studentTable.setOnMousePressed(e -> {
            controller.handleStudentClicked();
            addUpdateStudent.setText("Update");
            System.out.println("Clicked Student");
        });
        
        //adding students and editing them continue here
        addUpdateStudent.setOnAction(e -> {
            Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
            if (addUpdateStudent.getText().equals("Update")) {
                controller.handleEditStudent();
                //Set the selections to null
                studentTable.getSelectionModel().select(null);
                studentFirstNameInput.setText("");
                studentLastNameInput.setText("");
                //Combobox won't be cleared
                studentRoleInput.setText("");
                addUpdateStudent.setText("Add");
                
            }
            else{
                controller.handleAddStudent();
                addUpdateProject.setText("Add");
            }
        });
        //Clearing selected student
        clearStudent.setOnAction(e -> {
            //Set the selections to null
            studentTable.getSelectionModel().select(null);
            studentFirstNameInput.setText("");
            studentLastNameInput.setText("");
            studentRoleInput.setText("");
            addUpdateStudent.setText("Add");
            System.out.println("Student info cleared");
            
        });
        //deleting students
        removeStudent.setOnAction(e -> {
            controller.deleteStudent();
            studentTable.getSelectionModel().select(null);
            studentFirstNameInput.setText("");
            studentLastNameInput.setText("");
            studentRoleInput.setText("");
            addUpdateStudent.setText("Add");
            System.out.println("Student removed");
            
        });        
        studentTable.setOnKeyPressed(e -> {
            controller.handleKeyPressStudent(e.getCode());
        });
        
        
    }
     //Getters
    public TableView getProjectTable(){
        return projectTable;
    }
    public TableView getStudentTable(){
        return studentTable;
    }
    
    public void initRecitationComponent(){
        //Has all of the gui for the course details page
        //Strings from XML file
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //Section one
        String recitationTitle = props.getProperty(TAManagerProp.RECITATION_TITLE.toString());
        String sectionTitle = props.getProperty(TAManagerProp.SECTION_TITLE.toString());
        String instructorTitle = props.getProperty(TAManagerProp.INSTRUCTOR_TITLE.toString());
        String dayAndTimeTitle = props.getProperty(TAManagerProp.DAY_AND_TIME_TITLE.toString());
        String locationTitle = props.getProperty(TAManagerProp.LOCATION_TITLE.toString());
        String TATitle = props.getProperty(TAManagerProp.TA_TITLE.toString());
        
        //Section two
        String editTitle = props.getProperty(TAManagerProp.EDIT_TITLE.toString());
        //Use the section - location title for these controls
        String supervisingTATitle = props.getProperty(TAManagerProp.SUPERVISING_TA_TITLE.toString());
        String updateButton = props.getProperty(TAManagerProp.ADD_UPDATE_BUTTON_TEXT.toString());
        String clearButton = props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString());
        
        //Box to hold all the sections
        BorderPane border = new BorderPane();
        VBox recitationPane = new VBox();
        recitationPane.getStyleClass().add("background_pane");
        recitationPane.setPadding(new Insets(50,100,50,100));
        border.setCenter(recitationPane);
        
        //The first section
        VBox recitations = new VBox();  
        recitations.setPadding(new Insets(0,0,50,0));
        recitationPane.getChildren().add(recitations);
        
        //Box for title
        HBox recitationsInfoTitle = new HBox();
        recitationsInfoTitle.getStyleClass().add("hbox");
        recitationsInfoTitle.setPadding(new Insets(0,0,10,0));
        Label recitationsInfoLabel = new Label(recitationTitle);
        recitationsInfoLabel.setPadding(new Insets(0,50,0,0));
        recitationsInfoLabel.getStyleClass().add("title_text");
        recitationsInfoLabel.setAlignment(Pos.CENTER);
        
        //Button to remove 
        Button removeRecitation = new Button("-");
        removeRecitation.getStyleClass().add("title_text");
        removeRecitation.setAlignment(Pos.CENTER_RIGHT);
        recitationsInfoTitle.getChildren().add(recitationsInfoLabel);
        recitationsInfoTitle.getChildren().add(removeRecitation);
        //removeRecitation.prefHeightProperty().bind(recitationsInfoTitle.heightProperty().multiply(1));
        recitations.getChildren().add(recitationsInfoTitle);
        
        //Box to hold table
        HBox tablePane = new HBox();
        tablePane.getStyleClass().add("hbox");
        
        
        //Table for the recitations
        recitationList = new TableView();
        TAData data = (TAData)app.getDataComponent();
        ObservableList<Recitation> recitationData = data.getRecitations();
        recitationList.setItems(recitationData);
        TableColumn sectionColumn = new TableColumn(sectionTitle);
        TableColumn instructorColumn = new TableColumn(instructorTitle);
        TableColumn dayTimeColumn = new TableColumn(dayAndTimeTitle);
        TableColumn locationColumn = new TableColumn(locationTitle);
        TableColumn firstTAColumn = new TableColumn(TATitle);
        TableColumn secondTAColumn = new TableColumn(TATitle);
        sectionColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation, StringProperty>("section")
        );
        instructorColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation,String>("instructor")
        );
        dayTimeColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation,String>("date")
        );
        locationColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation,String>("location")
        );
        firstTAColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation,String>("TAOne")
        );
        secondTAColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation,String>("TATwo")
        );
        
        recitationList.setItems(recitationData);
        recitationList.getColumns().addAll(sectionColumn,instructorColumn,dayTimeColumn,
                locationColumn,firstTAColumn,secondTAColumn);
        recitationList.prefWidthProperty().bind(recitationsInfoTitle.widthProperty().multiply(1));
        tablePane.getChildren().add(recitationList);
        recitations.getChildren().add(tablePane);
        
        //Section two
        VBox editSpace = new VBox();  
        editSpace.setPadding(new Insets(0,0,0,0));
        //recitations.getStyleClass().add("background_pane");
        recitationPane.getChildren().add(editSpace);
        
        //Box for title
        HBox editTitleLine = new HBox();
        editTitleLine.getStyleClass().add("hbox");
        editTitleLine.setPadding(new Insets(0,0,10,0));
        Label editTitleLabel = new Label(editTitle);
        editTitleLabel.setPadding(new Insets(0,50,0,0));
        editTitleLabel.getStyleClass().add("title_text");
        editTitleLabel.setAlignment(Pos.CENTER);
        editTitleLine.getChildren().add(editTitleLabel);
        editSpace.getChildren().add(editTitleLine);
        
        //Controls
        //Box for the first row of controls
        HBox firstLine = new HBox();
        firstLine.getStyleClass().add("hbox");
        //For the subject
        HBox sectionControl = new HBox();
        Label sectionLabel = new Label(sectionTitle);
        recitationSectionInput = new TextField();
        sectionControl.getChildren().add(sectionLabel);
        sectionControl.getChildren().add(recitationSectionInput);
        sectionControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        sectionLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        sectionLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        recitationSectionInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //For the number
        HBox locationControl = new HBox();
        Label locationLabel = new Label(locationTitle);
        recitationLocationInput = new TextField();
        locationControl.getChildren().add(locationLabel);
        locationControl.getChildren().add(recitationLocationInput);
        locationControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        locationLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        locationLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        recitationLocationInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //Add them 
        firstLine.getChildren().add(sectionControl);
        firstLine.getChildren().add(locationControl);
        locationControl.setAlignment(Pos.CENTER_RIGHT);
        firstLine.setPadding(new Insets(0,0,5,0));
        firstLine.getStyleClass().add("hbox");
        editSpace.getChildren().add(firstLine);
        
        //Box for the second row of controls
        HBox secondLine = new HBox();
        secondLine.getStyleClass().add("hbox");
        //For the subject
        HBox instructorControl = new HBox();
        Label instructorLabel = new Label(instructorTitle);
        recitationInstructorInput = new TextField();
        instructorControl.getChildren().add(instructorLabel);
        instructorControl.getChildren().add(recitationInstructorInput);
        instructorControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        instructorLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        instructorLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        recitationInstructorInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //For the number
        HBox TAOneControl = new HBox();
        Label TAOneLabel = new Label(TATitle);
        //Creates the list for the TAInput
        TANamesListOne = FXCollections.observableArrayList();
        TANamesListOne.add("None");

        recitationTAOneInput = new ComboBox(TANamesListOne);
        recitationTAOneInput.setOnMousePressed(new EventHandler<MouseEvent>(){
            @Override
             public void handle(MouseEvent event) {
                 recitationTAOneInput.requestFocus();
                 controller.updateTAComboBoxOne(recitationTAOneInput);
            }
        });
        
        TAOneControl.getChildren().add(TAOneLabel);
        TAOneControl.getChildren().add(recitationTAOneInput);
        TAOneControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        TAOneLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        TAOneLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        recitationTAOneInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //Add them 
        secondLine.getChildren().add(instructorControl);
        secondLine.getChildren().add(TAOneControl);
        TAOneControl.setAlignment(Pos.CENTER_RIGHT);
        secondLine.setPadding(new Insets(0,0,5,0));
        secondLine.getStyleClass().add("hbox");
        editSpace.getChildren().add(secondLine);
        
        //Box for the second row of controls
        HBox thirdLine = new HBox();
        secondLine.getStyleClass().add("hbox");
        //For the subject
        HBox dayTimeControl = new HBox();
        Label dayTimeLabel = new Label(dayAndTimeTitle);
        recitationDateInput = new TextField();
        dayTimeControl.getChildren().add(dayTimeLabel);
        dayTimeControl.getChildren().add(recitationDateInput);
        dayTimeControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        dayTimeLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        dayTimeLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        recitationDateInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //For the number
        HBox TATwoControl = new HBox();
        Label TATwoLabel = new Label(TATitle);        
        TANamesListTwo = FXCollections.observableArrayList();
        TANamesListTwo.add("None");
        recitationTATwoInput = new ComboBox(TANamesListTwo);
        recitationTATwoInput.setOnMousePressed(new EventHandler<MouseEvent>(){
            @Override
             public void handle(MouseEvent event) {
                 recitationTATwoInput.requestFocus();
                 controller.updateTAComboBoxTwo(recitationTATwoInput);
            }
        });
        TATwoControl.getChildren().add(TATwoLabel);
        TATwoControl.getChildren().add(recitationTATwoInput);
        TATwoControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        TATwoLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        TATwoLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        recitationTATwoInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //Add them 
        thirdLine.getChildren().add(dayTimeControl);
        thirdLine.getChildren().add(TATwoControl);
        TATwoControl.setAlignment(Pos.CENTER_RIGHT);
        thirdLine.setPadding(new Insets(0,0,5,0));
        thirdLine.getStyleClass().add("hbox");
        editSpace.getChildren().add(thirdLine);
        //Fourth line for the buttons
        HBox fourthLine = new HBox();
        firstLine.getStyleClass().add("hbox");
        //For the subject
        Button add = new Button(updateButton);
        HBox divider = new HBox();
        divider.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        Button clear = new Button(clearButton);
        add.setMinWidth(10);
        clear.setMinWidth(10);
        //add.setPadding(new Insets(0,0,0,10));
        fourthLine.getChildren().addAll(add,divider,clear);
        editSpace.getChildren().add(fourthLine);
        fourthLine.getStyleClass().add("hbox");
        super.recitationTab.setContent(border);  
        
        //Stuff for the controller (recitations)
        
        recitationList.setOnMousePressed(e -> {
            controller.handleRecitationClicked();
            add.setText("Update");
            System.out.println("Clicked Recitation");
        });
        
        
        
        //adding recitations and editing
        add.setOnAction(e -> {
            Object selectedItem = recitationList.getSelectionModel().getSelectedItem();
            if (add.getText().equals("Update")) {
                controller.handleEditRecitation();
                //Set the selections to null
                recitationList.getSelectionModel().select(null);
                recitationSectionInput.setText("");
                recitationInstructorInput.setText("");
                recitationDateInput.setText("");
                recitationLocationInput.setText("");
                //Combobox won't be cleared
                add.setText("Add");
                
            }
            else{
                controller.handleAddRecitation();
                add.setText("Add");
            }
        });
        
        //Clearing selected recitation
        
        clear.setOnAction(e -> {
            //Set the selections to null
            recitationList.getSelectionModel().select(null);
            recitationSectionInput.setText("");
            recitationInstructorInput.setText("");
            recitationDateInput.setText("");
            recitationLocationInput.setText("");
            //Ignore comboboxes
            add.setText("Add");
            System.out.println("Recitation info cleared");
            
        });
        //deleting recitations
        removeRecitation.setOnAction(e -> {
            controller.deleteRecitation();
            //Set the selections to null
            recitationList.getSelectionModel().select(null);
            recitationSectionInput.setText("");
            recitationInstructorInput.setText("");
            recitationDateInput.setText("");
            recitationLocationInput.setText("");
            //Ignore comboboxes
            add.setText("Add");
            System.out.println("Recitation removed");
            
        });
        
        recitationList.setOnKeyPressed(e -> {
            controller.handleKeyPressRecitation(e.getCode());
        });
        
        //Undo
        
        recitationPane.setOnKeyPressed(e -> {
            if (e.isControlDown() && e.getCode() == (KeyCode.Y)) {
                System.out.println("Recitation Control Y");
                controller.handleReDoTransaction();
            } else if (e.isControlDown() && e.getCode() == (KeyCode.Z)) {
                System.out.println("Recitation Control Z Pressed");
                controller.handleUndoTransaction();
            }
        });
        


        
    }
    
    public TableView getRecitationList(){
        return recitationList;
    }
    public void initScheduleComponent(){
        //Has all of the gui for the course details page
        //Strings from XML file
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        //Title
        String sheduleTitle = props.getProperty(TAManagerProp.SCHEDULE_TITLE.toString());
        //Section one
        String calendarBoundaries = props.getProperty(TAManagerProp.CALENDAR_BOUNDARIES.toString());
        String startingDate = props.getProperty(TAManagerProp.STARTING_DATE.toString());
        String endingDate = props.getProperty(TAManagerProp.ENDING_DATE.toString());
        
        //Section two
        String scheduleItemsTitle = props.getProperty(TAManagerProp.SCHEDULE_ITEMS_TITLE.toString());
        String type = props.getProperty(TAManagerProp.TYPE_TITLE.toString());
        String date = props.getProperty(TAManagerProp.DATE_TITLE.toString());
        String title = props.getProperty(TAManagerProp.TITLE_INFO_TEXT.toString());
        String topic = props.getProperty(TAManagerProp.TOPIC_TITLE.toString());
        String addTitle = props.getProperty(TAManagerProp.EDIT_TITLE.toString());
        //String type - topic can be reused
        String time = props.getProperty(TAManagerProp.TIME_TITLE.toString());
        String link = props.getProperty(TAManagerProp.LINK_TITLE.toString());
        String criteria = props.getProperty(TAManagerProp.CRITERIA_TITLE.toString());
        String addButton = props.getProperty(TAManagerProp.ADD_UPDATE_BUTTON_TEXT.toString());
        String clearButton = props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString());
                
        //Box to hold all the sections
        BorderPane border = new BorderPane();
        VBox schedulePane = new VBox();
        schedulePane.getStyleClass().add("background_pane");
        schedulePane.setPadding(new Insets(50,100,50,100));
        border.setCenter(schedulePane);
        ScrollPane scrollPane = new ScrollPane();
        border.prefWidthProperty().bind(scrollPane.widthProperty().multiply(1));
        
        //The first section
        VBox calendarBoundariesPane = new VBox();
        calendarBoundariesPane.setPadding(new Insets(0,0,25,0));
        calendarBoundariesPane.getStyleClass().add("hbox");
        schedulePane.getChildren().add(calendarBoundariesPane);
        //Title
        Label scheduleLabel = new Label(sheduleTitle);
        scheduleLabel.getStyleClass().add("title_text");
        VBox titleBox = new VBox();
        titleBox.getChildren().add(scheduleLabel);
        titleBox.setPadding(new Insets(0,0,10,0));
        calendarBoundariesPane.getChildren().add(titleBox);
        //Hbox for controls
        HBox calendarControls = new HBox();
        calendarControls.getStyleClass().add("hbox");
        calendarControls.prefWidthProperty().bind(schedulePane.widthProperty().multiply(1));
        HBox startingDateControl = new HBox();
        Label startingDateLabel = new Label(startingDate);
        startingDateInput = new DatePicker();
        startingDateLabel.prefWidthProperty().bind(schedulePane.widthProperty().multiply(.1));
        startingDateInput.prefWidthProperty().bind(schedulePane.widthProperty().multiply(.2));
        startingDateControl.getChildren().addAll(startingDateLabel,startingDateInput);
        HBox endingDateControl = new HBox();
        Label endingDateLabel = new Label(endingDate);
        endingDateInput = new DatePicker();
        endingDateControl.getChildren().add(endingDateLabel);
        endingDateControl.getChildren().add(endingDateInput);
        endingDateLabel.prefWidthProperty().bind(schedulePane.widthProperty().multiply(.1));
        endingDateInput.prefWidthProperty().bind(schedulePane.widthProperty().multiply(.2));
        startingDateControl.setPadding(new Insets (0,40,0,0));
        calendarControls.getChildren().addAll(startingDateControl,endingDateControl);
        Button updateDates = new Button("Update Dates");
        HBox updateBox = new HBox();
        updateBox.getChildren().add(updateDates);
        //add
        calendarControls.prefWidthProperty().bind(calendarBoundariesPane.widthProperty().multiply(1));
        calendarBoundariesPane.getChildren().addAll(calendarControls,updateBox);
        



        //Second Section
        VBox scheduleItemsPane = new VBox();
        scheduleItemsPane.setPadding(new Insets(0,0,0,0));
        
        //Title
        HBox scheduleInfoTitle = new HBox();
        Button removeScheduleItem = new Button("-");
        removeScheduleItem.setAlignment(Pos.CENTER);
        scheduleInfoTitle.getStyleClass().add("hbox");
        scheduleInfoTitle.setPadding(new Insets(0,0,0,0));
        Label scheduleInfoLabel = new Label(scheduleItemsTitle);
        scheduleInfoLabel.getStyleClass().add("title_text");
        scheduleInfoLabel.prefWidthProperty().bind(calendarBoundariesPane.widthProperty().multiply(.25));
        scheduleInfoTitle.getChildren().add(scheduleInfoLabel);
        scheduleInfoTitle.getChildren().add(removeScheduleItem);
        scheduleItemsPane.getChildren().add(scheduleInfoTitle);
        //Table
        HBox tablePane = new HBox();
        tablePane.getStyleClass().add("hbox");
        //Tabel for the recitations
        scheduleTable = new TableView();
        TableColumn typeColumn = new TableColumn(type);
        TableColumn dateColumn = new TableColumn(date);
        TableColumn titleColumn = new TableColumn(title);
        TableColumn topicColumn = new TableColumn(topic);
        TAData data = (TAData)app.getDataComponent();
        //Load the data into the table
        ObservableList<Schedule> scheduleData = data.getSchedules();
        typeColumn.setCellValueFactory(
            new PropertyValueFactory<Schedule,String>("type")
        );
        dateColumn.setCellValueFactory(
            new PropertyValueFactory<Schedule,String>("date")
        );
        titleColumn.setCellValueFactory(
            new PropertyValueFactory<Schedule,String>("title")
        );
        topicColumn.setCellValueFactory(
            new PropertyValueFactory<Schedule,String>("topic")
        );
        scheduleTable.setItems(scheduleData);
        
        scheduleTable.getColumns().addAll(typeColumn,dateColumn,titleColumn,topicColumn);
        scheduleTable.prefWidthProperty().bind(calendarBoundariesPane.widthProperty().multiply(1));
        tablePane.getChildren().add(scheduleTable);
        scheduleItemsPane.getChildren().add(tablePane);
        
        //add edit section
        //title
        HBox editTitleLine = new HBox();
        editTitleLine.getStyleClass().add("hbox");
        editTitleLine.setPadding(new Insets(0,0,0,0));
        Label editTitleLabel = new Label(addTitle);
        editTitleLine.getChildren().add(editTitleLabel);
        scheduleItemsPane.getChildren().add(editTitleLine);
        //controls
        //Box for the first row of controls
        HBox firstLine = new HBox();
        firstLine.getStyleClass().add("hbox");
        firstLine.setPadding(new Insets(0,0,0,0));
        //For the subject
        HBox typeControl = new HBox();
        Label typeLabel = new Label(type);
        //insert schedule types here
        ObservableList<String> scheduleTypes = FXCollections.observableArrayList(
            "Exam",
            "Holiday",
            "Snow day",
            "Cancelled Class",
            "HW"
        );
        scheduleTypeInput = new ComboBox(scheduleTypes);
        
        scheduleTypeInput.setOnMousePressed(new EventHandler<MouseEvent>(){
            @Override
             public void handle(MouseEvent event) {
                 scheduleTypeInput.requestFocus();
            }
        });
        typeControl.getChildren().add(typeLabel);
        typeControl.getChildren().add(scheduleTypeInput);
        typeControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        typeLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        typeLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        scheduleTypeInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //For the number
        HBox dateControl = new HBox();
        Label dateLabel = new Label(date);
        scheduleDateInput = new DatePicker();
        dateControl.getChildren().add(dateLabel);
        dateControl.getChildren().add(scheduleDateInput);
        dateControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        dateLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        dateLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        scheduleDateInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        firstLine.getChildren().addAll(typeControl,dateControl);
        
        //Box for the second row of controls
        HBox secondLine = new HBox();
        secondLine.getStyleClass().add("hbox");
        secondLine.setPadding(new Insets(0,0,0,0));
        //For the subject
        HBox timeControl = new HBox();
        Label timeLabel = new Label(time);
        scheduleTimeInput = new TextField();
        timeControl.getChildren().add(timeLabel);
        timeControl.getChildren().add(scheduleTimeInput);
        timeControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        timeLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        timeLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        scheduleTimeInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //For the number
        HBox titleControl = new HBox();
        Label titleLabel = new Label(title);
        scheduleTitleInput = new TextField();
        titleControl.getChildren().add(titleLabel);
        titleControl.getChildren().add(scheduleTitleInput);
        titleControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        titleLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        titleLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        scheduleTitleInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        secondLine.getChildren().addAll(timeControl,titleControl);
        //Third Row
        HBox thirdLine = new HBox();
        thirdLine.getStyleClass().add("hbox");
        thirdLine.setPadding(new Insets(0,0,0,0));
        //For the subject
        HBox topicControl = new HBox();
        Label topicLabel = new Label(topic);
        scheduleTopicInput = new TextField();
        topicControl.getChildren().add(topicLabel);
        topicControl.getChildren().add(scheduleTopicInput);
        topicControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        topicLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        topicLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        scheduleTopicInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //For the number
        HBox linkControl = new HBox();
        Label linkLabel = new Label(link);
        scheduleLinkInput = new TextField();
        linkControl.getChildren().add(linkLabel);
        linkControl.getChildren().add(scheduleLinkInput);
        linkControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        linkLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        linkLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        scheduleLinkInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        thirdLine.getChildren().addAll(topicControl,linkControl);
        //Fourth line
        //Third Row
        HBox fourthLine = new HBox();
        fourthLine.getStyleClass().add("hbox");
        fourthLine.setPadding(new Insets(0,0,0,0));
        //For the subject
        HBox criteriaControl = new HBox();
        Label criteriaLabel = new Label(criteria);
        scheduleCriteriaInput = new TextField();
        criteriaControl.getChildren().add(criteriaLabel);
        criteriaControl.getChildren().add(scheduleCriteriaInput);
        criteriaControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        criteriaLabel.prefWidthProperty().bind(firstLine.widthProperty().multiply(.1));
        criteriaLabel.prefHeightProperty().bind(firstLine.heightProperty().multiply(1));
        scheduleCriteriaInput.prefWidthProperty().bind(firstLine.widthProperty().multiply(.3));
        //For the number
        HBox buttonControl = new HBox();
        Button add = new Button(addButton);
        Button clear = new Button(clearButton);
        //HBox space = new HBox();
        buttonControl.getChildren().addAll(add,clear);
        buttonControl.prefWidthProperty().bind(firstLine.widthProperty().multiply(.5));
        //add.prefWidthProperty().bind(firstLine.widthProperty().multiply(.2));
        //space.prefHeightProperty().bind(firstLine.heightProperty().multiply(.1));
        //clear.prefWidthProperty().bind(firstLine.widthProperty().multiply(.2));
        buttonControl.setSpacing(20);
        fourthLine.getChildren().addAll(criteriaControl,buttonControl);
        scheduleItemsPane.getChildren().addAll(firstLine,secondLine,thirdLine,fourthLine);
        schedulePane.getChildren().add(scheduleItemsPane);
        scrollPane.setContent(border);
        super.scheduleTab.setContent(scrollPane);
        
        //Stuff for the controller (Schedules)
        scheduleTable.setOnMousePressed(e -> {
            controller.handleScheduleClicked();
            System.out.println("Clicked Schedule");
            add.setText("Update");
        });
        updateDates.setOnAction(e ->{
            controller.handleUpdateDates();
        });
        //adding scheduleItems and editing them continue here
        add.setOnAction(e -> {
            Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
            if (add.getText().equals("Update")) {
                controller.handleEditSchedule();
                //Set the selections to null
                scheduleTable.getSelectionModel().select(null);
                //Ignore type combobox
                scheduleTimeInput.setText("");
                scheduleTopicInput.setText("");
                scheduleCriteriaInput.setText("");
                scheduleDateInput.getEditor().clear();
                scheduleTitleInput.setText("");
                scheduleLinkInput.setText("");
                
                add.setText("Add");
            }
            else{
                controller.handleAddSchedule();
                add.setText("Add");
            }
        });
        //Clearing selected schedule
        clear.setOnAction(e -> {                
                //Set the selections to null
                scheduleTable.getSelectionModel().select(null);
                //Ignore type combobox
                scheduleTimeInput.setText("");
                scheduleTopicInput.setText("");
                scheduleCriteriaInput.setText("");
                scheduleDateInput.getEditor().clear();
                scheduleTitleInput.setText("");
                scheduleLinkInput.setText("");
                
                add.setText("Add");
            System.out.println("Schedule info cleared");
            
        });
        //deleting schedules
        removeScheduleItem.setOnAction(e -> {
            controller.deleteSchedule();
                          
                //Set the selections to null
                scheduleTable.getSelectionModel().select(null);
                //Ignore type combobox
                scheduleTimeInput.setText("");
                scheduleTopicInput.setText("");
                scheduleCriteriaInput.setText("");
                scheduleDateInput.getEditor().clear();
                scheduleTitleInput.setText("");
                scheduleLinkInput.setText("");
                
                add.setText("Add");
        });        
        scheduleTable.setOnKeyPressed(e -> {
            controller.handleKeyPressSchedule(e.getCode());
            
                //Set the selections to null
                scheduleTable.getSelectionModel().select(null);
                //Ignore type combobox
                scheduleTimeInput.setText("");
                scheduleTopicInput.setText("");
                scheduleCriteriaInput.setText("");
                scheduleDateInput.getEditor().clear();
                scheduleTitleInput.setText("");
                scheduleLinkInput.setText("");
                
        });
        //Undo
        
        schedulePane.setOnKeyPressed(e -> {
            if (e.isControlDown() && e.getCode() == (KeyCode.Y)) {
                System.out.println("Schedule Control Y");
                controller.handleReDoTransaction();
            } else if (e.isControlDown() && e.getCode() == (KeyCode.Z)) {
                System.out.println("Schedule Control Z Pressed");
                controller.handleUndoTransaction();
            }

        });
    }
}