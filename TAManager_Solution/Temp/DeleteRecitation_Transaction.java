/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import jtps.jTPS_Transaction;
import tam.data.Recitation;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class DeleteRecitation_Transaction implements jTPS_Transaction {

    private Recitation r;
    private TAData data;
    private ArrayList<String> keyArray;

    public DeleteRecitation_Transaction(Recitation r, TAData data) {
        keyArray = new ArrayList<String>();
        this.data = data;
        this.r = r; 

    }

    @Override
    public void doTransaction() {  //control Y 
        keyArray.clear(); //clear anystored values 
        String title = r.getDate();
        data.removeRecitation(title);
    }

    @Override
    public void undoTransaction() {
        data.addRecitation(r.getSection(),r.getInstructor(),r.getDate(), 
                r.getLocation(),r.getTAOne(),r.getTATwo());
    }
}
