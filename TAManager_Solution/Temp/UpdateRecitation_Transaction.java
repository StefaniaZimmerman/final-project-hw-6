/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;


import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam.data.Project;
import tam.data.TAData;
import tam.data.Recitation;


/**
 *
 * @author khurr
 */
public class UpdateRecitation_Transaction implements jTPS_Transaction {

    private String oldSection;
    private String newSection;
    private String oldInstructor;
    private String newInstructor;
    private String oldDate;
    private String newDate;
    private String oldLocation;
    private String newLocation;
    private String oldTAOne;
    private String newTAOne;
    private String oldTATwo;
    private String newTATwo;
    private TAData taData;
    private Recitation r;
    private TAManagerApp app; 
    private TAWorkspace transWorkspace; 

    public UpdateRecitation_Transaction(String orgSection, String section, 
            String orgInstructor, String instructor, 
            String orgDate, String date, 
            String orgLocation, String location,
            String orgTAOne, String TAOne,
            String orgTATwo, String TATwo,
            TAData data, TAManagerApp taApp, TAWorkspace workspace) {
        oldSection = orgSection;
        newSection = section;
        oldInstructor = orgInstructor;
        newInstructor = instructor;
        oldDate = orgDate;
        newDate = date;
        oldLocation = orgLocation;
        newLocation = location;
        oldTAOne = orgTAOne;
        newTAOne = TAOne;
        oldTATwo = orgTATwo;
        newTATwo = TATwo;
        taData = data;
        r = data.getRecitation(orgDate);
        app=taApp; 
        transWorkspace=workspace; 
    }

    @Override
    public void doTransaction() {  //Control Y 
        System.out.println("updateRecitation doTransaction ");
        Recitation recitation = taData.getRecitation(oldDate);
        recitation.setSection(newSection);
        recitation.setInstructor(newInstructor);
        recitation.setDate(newDate);
        recitation.setLocation(newLocation);
        recitation.setTAOne(newTAOne);
        recitation.setTATwo(newTATwo);
        
        TAController controller = new TAController(app);        
        r.setSection(newSection);
        r.setInstructor(newInstructor);
        r.setDate(newDate);
        r.setLocation(newLocation);
        r.setTAOne(newTAOne);
        r.setTATwo(newTATwo);

        transWorkspace.recitationList.refresh();
        
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("updateRecitation undoTransaction ");
        taData.getRecitation(newDate).setDate(oldDate);
        r.setSection(oldSection);
        r.setInstructor(oldInstructor);
        r.setDate(oldDate);
        r.setLocation(oldLocation);
        r.setTAOne(oldTAOne);
        r.setTATwo(oldTATwo);

        
        transWorkspace.recitationList.refresh();
        
        //transWorkspace.taTable.refresh();

    }

}
