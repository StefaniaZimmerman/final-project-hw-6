/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import djf.components.AppDataComponent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.file.TAFiles;

/**
 *
 * @author CleverAntix
 */
public class SaveTest {
    //TAManagerApp testApp;
    //TAData testData;
    //TAFiles testFile;
    
    public SaveTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }
    
    
    @Test
    public void testSaveData() throws Exception {
        //TAManagerApp testApp = new TAManagerApp();
        //testApp.buildAppComponenetsTest();
        TAData testData = new TAData();
        //testData = (TAData)testApp.getDataComponent();
        TAFiles testFile = new TAFiles();
        testData.setSubject("CSE 219");
        
        testData.setSemester("Spring");
        testData.setCourseNumber("02");
        testData.setYear("2017");
        testData.setCourseTitle("Computer Science II");
        testData.setInstructorName("Richard McKenna");
        testData.setInstructorHome("http://www3.cs.stonybrook.edu/~cse219/Section02/index.html");
        testData.setExportDirectory("EXPORT DIRECTORY TEST");
        //Site template
        testData.setTemplateDirectory("TEMPLATE DIRECTORY TEST");
        //Page style section
        testData.setBannerImage("bannerImage.jpeg");
        testData.setLeftFooterImage("leftFooter.jpeg");
        testData.setRightFooterImage("rightFooter.jpeg");
        testData.setStylesheet("CSS STYLESHEET TEST");
        
        //TA Data section
        testData.addTA("Stefania Zimmerman", "stefania.zimmerman@stonybrook.edu",true);
        testData.addTA("Jane Doe","jane.doe@stonybrook.edu",true);
        testData.addTA("Joe Smith","joe.smith@stonybrook.edu",false);
        //testData.addOfficeHoursReservation("MONDAY", "11_00am", "Stefania Zimmerman");
        //testData.addOfficeHoursReservation("WEDNESDAY","4_00pm","Jane Doe");
        //testData.addOfficeHoursReservation("FRIDAY","9_00am","Joe Smith");
        
        //Recitations section
        testData.addRecitation("02","Richard McKenna","Friday,2:00pm","Old CS Building 2007","Jane Doe","Joe Smith");
        testData.addRecitation("01", "Richard McKenna", "Wednesday,5:00pm","New CS Building 209", "Stefania Zimmerman", "Jane Doe");
        
        //Schedule Section
        testData.setStartingDate("01/01/2017");
        testData.setEndingDate("05/06/2017");
        //Add some schedule items
        testData.addSchedule("Holiday","2/14/2017", "Valentine's Day", "Holiday","holiday.html","criteria","All Day");
        testData.addSchedule("Exam","3/24/2017","Midterm","All course material","exam.html","criteria","2:00 PM");
        
        //Project section
        testData.addProject("Test Project","FFFFFF","000000","testProject.html");
        testData.addStudent("Stefania","Zimmerman","Test Project","Designer");
        testData.addStudent("Jane","Doe","Test Project","Coder");
        //Hard code all the data
        
        System.out.println("saveData");
        AppDataComponent data = testData;
        String filePath = "SiteSaveTest.json";
        TAFiles instance = testFile;
        instance.saveData(data, filePath);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

}
